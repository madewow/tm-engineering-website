$(document).foundation();

$('#main-navigation').find('li').on('mouseover', function() {
	if ($(this).find('> ul').length > 0) {
		$(this).addClass('show');
	}
	
}).on('mouseout', function() {
	if ($(this).find('ul').length > 0) {
		$(this).removeClass('show');
	}
});

$('#side-navigation').find('.trigger').on('click', function() {
	if (! $(this).parent().hasClass('show')) {
		$(this).parent().addClass('show');
		
		$(this).find('i').removeClass().addClass('fas fa-chevron-up');
		
	} else {
		$(this).parent().removeClass('show');
		
		$(this).find('i').removeClass().addClass('fas fa-chevron-down');
	}
})


$('#subscription-form').bind('submit', function(e) {
	e.preventDefault();
	
}).on('invalid.zf.abide', function(ev, elm){
	console.log(ev);
		
}).on('formvalid.zf.abide', function(ev, elm){
	$('#subscription-form').find('input').attr('disabled', true);
	$('#contact-form').find('button').attr('disabled', true).addClass('sending');
	$('#contact-form').find('span').text('Sending');
	
	thisForm = $('#subscription-form');
    formData = thisForm.serialize();
    console.log(formData);

    var email = $("#email-footer").val();
	
	$.ajax({
        type: 'POST',
        url: 'http://tm-engineering.madejkt.com/admin/api/subscription_form?key=d9d9b483f3b94c962f64424ce03d84ae0d330b0b', //
        data: {email:email},
        success: function(response){
        	// response=JSON.parse(response);

        	alert('Success, Message Sent');

		   	$("#email-footer").val('');
        	
        	$('#subscription-form').find('input').attr('disabled', false);
			$('#subscription-form').find('button').attr('disabled', false).removeClass('sending');
			$('#subscription-form').find('span').text('Submit');
        }
	});
});


$('#contact-form-page').bind('submit', function(e) {
	e.preventDefault();
	
}).on('invalid.zf.abide', function(ev, elm){
	console.log(ev);
		
}).on('formvalid.zf.abide', function(ev, elm){
	$('#contact-form').find('input').attr('disabled', true);
	$('#contact-form').find('textarea').attr('disabled', true);
	$('#contact-form').find('button').attr('disabled', true).addClass('sending');
	$('#contact-form').find('span').text('Sending');
	
	thisForm = $('#contact-form');
    formData = thisForm.serialize();
    console.log(formData);

    var name = $("#name").val();
    var email = $("#email").val();
    var detail = $("#detail").val();
	
	$.ajax({
        type: 'POST',
        url: 'http://tm-engineering.madejkt.com/admin/api/contact_form?key=d9d9b483f3b94c962f64424ce03d84ae0d330b0b', //
        data: {name: name, email:email, detail:detail},
        success: function(response){
        	// response=JSON.parse(response);;

        	$("#alert-message").removeClass('hide');
        	$("#alert-message").addClass('show');

       		$("#name").val('');
		   	$("#email").val('');
		    $("#detail").val('');
        	
        	$('#contact-form').find('input').attr('disabled', false);
			$('#contact-form').find('textarea').attr('disabled', false);
			$('#contact-form').find('button').attr('disabled', false).removeClass('sending');
			$('#contact-form').find('span').text('Submit');
			$('#contact-form').find('.callout').removeClass('hide');
        }
	});
});

$('#product-specialist-form').bind('submit', function(e) {
	e.preventDefault();
	
}).on('invalid.zf.abide', function(ev, elm){
	console.log(ev);
		
}).on('formvalid.zf.abide', function(ev, elm){
	$('#product-specialist-form').find('input').attr('disabled', true);
	$('#product-specialist-form').find('select').attr('disabled', true);
	$('#product-specialist-form').find('textarea').attr('disabled', true);
	$('#product-specialist-form').find('button').attr('disabled', true).addClass('sending');
	$('#product-specialist-form').find('span').text('Sending');
	
	thisForm = $('#product-specialist-form');
    formData = thisForm.serialize();
    console.log(formData);

    var first_name 	= $("#first_name").val();
    var last_name 	= $("#last_name").val();
    var email 		= $("#email").val();
    var phone 		= $("#phone").val();
    var company 	= $("#company").val();
    var purchase_timeline = $("#purchase_timeline").val();
    var notes 		= $("#detail").val();
	
	$.ajax({
        type: 'POST',
        url: 'http://tm-engineering.madejkt.com/admin/api/specialist_form?key=d9d9b483f3b94c962f64424ce03d84ae0d330b0b', //
        data: {firstname: first_name, lastname:last_name, email:email, phone:phone, company:company, purchase_timeline:purchase_timeline, notes:notes},
        success: function(response){
        	// response=JSON.parse(response);

        	// alert('contact specialist success');
        	$("#alert-message").removeClass('hide');
        	$("#alert-message").addClass('show');

       		$("#first_name").val('');
		 	$("#last_name").val('');
			$("#email").val('');
			$("#phone").val('');
			$("#company").val('');
		    $("#purchase_timeline").val('');
			$("#detail").val('');
        	
        	$('#product-specialist-form').find('input').attr('disabled', false);
        	$('#product-specialist-form').find('select').attr('disabled', false);
			$('#product-specialist-form').find('textarea').attr('disabled', false);
			$('#product-specialist-form').find('button').attr('disabled', false).removeClass('sending');
			$('#product-specialist-form').find('span').text('Submit');
			$('#product-specialist-form').find('.callout').removeClass('hide');
        }
	});
});