<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

	public function __construct()
    {
        $this->load->database();
    }

    public function find_all_by($table=null, $conditions=array(), $order=array()) {
        
        if(!empty($conditions))
        {
            //let's setup our field/value check
            foreach($conditions as $k => $v)
                $this->db->where($k,$v);
        }

        if(!empty($order))
        	$this->db->order_by($order[0], $order[1]);

        $this->db->from($table);
        return $this->db->get()->result();
    }

}