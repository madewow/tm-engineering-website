<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('getview');

        $this->data['current'] = "";
        
        if ($this->uri->uri_string() != "") {
        	$this->data['current'] = $this->uri->segment_array()[1];
        	$this->data['current_sub'] = $this->uri->segment_array()[3];
        }

        $this->load->model('main_model');

        $categories = $this->main_model->find_all_by('category', array('deleted' => '0'), array('name', 'asc'));   

        foreach ($categories as $key => $c) 
		{
			$where =  array('category_id' => $c->id, 'deleted' => '0');
			$categories[$key]->subcategories = $this->main_model->find_all_by('category_sub', $where);
		}

        $this->data['menu_categories'] = $categories;
    }

	public function index()
	{
		$data = '';

		$products = $this->main_model->find_all_by('category_sub', array('category_id' => '16', 'deleted' => '0'), array('name', 'asc'));
		$this->data['products'] = $products;

		$html = '';

		foreach ($products as $key => $p) {

        $html .= '<a class="cell" href="'.base_url('product/subcategory/'.$p->id.'/'.url_title($p->name, '-', TRUE)).'">
		            <p>'.$p->name.'</p>
		            <img src="'.$this->config->item('admin_url').'/data/images/'.$p->featured_image.'">
		        </a>';
        }

        $content = $this->load->view('pages/home', $data, TRUE);
        $content = str_replace('{list_product}', $html, $content);

		$this->data['title'] = "Welcome to TM Engineering";
		$this->data['page-id'] = "home-page";
		$this->data['page-styles'] = 'pages/styles/home';
		$this->data['page-scripts'] = 'pages/scripts/home';

		$this->data['content'] = $content;

		$this->getview->render($this->data);
	}
    
    public function product() {
		$data = '';

		$categories = $this->main_model->find_all_by('category', array('deleted' => '0'), array('name', 'asc'));

		foreach ($categories as $key => $c) 
		{
			$where =  array('category_id' => $c->id, 'deleted' => '0');
			$categories[$key]->subs = $this->main_model->find_all_by('category_sub', $where);
		}

		$html = '';

		foreach ($categories as $key => $c) {

			$html .= '<section>
			    <header>
			         <div class="title-bar">
			            <div class="grid-container">
			                <div class="grid-x grid-padding-x">
			                    <div class="cell">
			                        <h2>'.$c->name.'</h2>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </header>';

			if(empty($categories[$key]->subs)) { 
			$html .= '
			    <div class="product-list">
			        <div class="grid-container">
			            empty list
			        </div>
			    </div>';

			}

			$html .='<div class="product-list">
			        <div class="grid-container">
			            <div class="grid-x grid-padding-x xsmall-up-2 medium-up-3 large-up-4">';

			        	foreach ($categories[$key]->subs as $key_sub => $s) { 

			            $html .= '<a class="cell" href="'.base_url('product/subcategory/'.$s->id.'/'.url_title($s->name, '-', TRUE)).'">
			                    <p>'.$s->name.'</p>
			                    <img src="'.$this->config->item('admin_url').'/data/images/'.$s->featured_image.'">
			                </a>';
			            } 
			$html .='
			            </div>
			        </div>
			    </div>

			</section>';

		}

		$this->data['categories'] = $categories;

		$this->data['title'] = "Subcategory - TM Engineering";
		$this->data['page-id'] = "category-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $html;

		$this->getview->render($this->data);
	}
	
	public function subcategory($id=null, $name=null) {
		$data = '';

		$subcategory = $this->main_model->find_all_by('category_sub', array('id' => $id, 'deleted' => '0'), array('name', 'asc'));

		foreach ($subcategory as $key => $s) 
		{
			$where =  array('category_sub_id' => $s->id, 'deleted' => '0', 'status' => 'publish');
			$subcategory[$key]->products = $this->main_model->find_all_by('product', $where);
		}

		$subcategory = $subcategory[0];

		$html = '';
		foreach ($subcategory->products as $key_product => $p) { 
	        $html .= '<a class="cell" href="'.base_url('product/product-detail/'.$p->id.'/'.url_title($p->name, '-', TRUE)).'">
	            <p>'.$p->name.'</p>
	            <img src="'.$this->config->item('admin_url').'/data/images/'.$p->featured_image.'">
	        </a>';
        }

		$content = $this->load->view('pages/subcategory', $data, TRUE);
		$content = str_replace('{name}', $subcategory->name, $content);
		$content = str_replace('{description}', $subcategory->description, $content);
		$content = str_replace('{lists}', $html, $content);
		$content = str_replace('{featured_image}', $this->config->item('admin_url').'/data/images/'.$subcategory->featured_image, $content);

		$this->data['title'] = "Category - TM Engineering";
		$this->data['page-id'] = "subcategory-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $content;

		$this->getview->render($this->data);
	}
	
	public function product_detail($id=null, $name=null) {
		$data = '';

		$product = $this->main_model->find_all_by('product', array('id' => $id, 'deleted' => '0'), array('name', 'asc'));

		foreach ($product as $key => $s) 
		{
			$where =  array('product_id' => $s->id, 'deleted' => '0');
			$product[$key]->files 	= $this->main_model->find_all_by('product_file', $where);
			$product[$key]->images 	= $this->main_model->find_all_by('product_image', $where);
		}

		$this->data['product'] = $product[0];
		$product = $product[0];

		$files = "";

		foreach ($product->files as $key => $f) { 
			$files .= '<a target="_blank" href="'.$this->config->item('admin_url').'/data/files/'.$f->name.'">'.$f->name.'</a><br>';
        } 

        foreach ($product->images as $key => $i) { 

            $images .= '<div class="cell xsmall-12 text-center">
                <img src="'.$this->config->item('admin_url').'/data/images/'.$i->name.'">
            </div>';
        }

        $subtitle = $this->main_model->find_all_by('category_sub', array('id' => $product->category_sub_id, 'deleted' => '0'), array('name', 'asc'));

        // var_dump($product);die;

        if(!empty($files)){
        	$additional_details = "Additional Details";
        }
        else{
        	$additional_details = "";
        }

		$content = $this->load->view('pages/product-detail', $data, TRUE);
		$content = str_replace('{name}', $product->name, $content);
		$content = str_replace('{url_back}', base_url('product/subcategory/'.$subtitle[0]->id.'/'.url_title($subtitle[0]->name, '-', TRUE)), $content);
		$content = str_replace('{subtitle}', $subtitle[0]->name, $content);
		$content = str_replace('{excerpt_title}', $product->excerpt_title, $content);
		$content = str_replace('{excerpt}', $product->excerpt, $content);
		$content = str_replace('{additional_details}', $additional_details, $content);
		$content = str_replace('{files}', $files, $content);
		$content = str_replace('{images}', $images, $content);
		$content = str_replace('{content}', $product->content, $content);
		$content = str_replace('{featured_image}', $this->config->item('admin_url').'/data/images/'.$product->featured_image, $content);

		$this->data['title'] = "Product Detail - TM Engineering";
		$this->data['page-id'] = "product-detail-page";
		$this->data['page-styles'] = 'pages/styles/product-detail';
		$this->data['page-scripts'] = 'pages/scripts/product-detail';

		$this->data['content'] = $content;

		$this->getview->render($this->data);
	}
	
	public function about_us()
	{
		$data = '';

		$this->data['title'] = "About TM Engineering";
		$this->data['page-id'] = "about-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $this->load->view('pages/about-us', $data, TRUE);

		$this->getview->render($this->data);
	}
	
	public function service_repair()
	{
		$data = '';

		$this->data['title'] = "Service & Repiar TM Engineering";
		$this->data['page-id'] = "about-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $this->load->view('pages/service-repair', $data, TRUE);

		$this->getview->render($this->data);
	}
	
	public function contact()
	{
		$data = '';

		$this->data['title'] = "Contact TM Engineering";
		$this->data['page-id'] = "contact-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $this->load->view('pages/contact', $data, TRUE);

		$this->getview->render($this->data);
	}
	
	public function terms_conditions()
	{
		$data = '';

		$this->data['title'] = "TM Engineering Terms & Conditions";
		$this->data['page-id'] = "generic-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $this->load->view('pages/terms-conditions', $data, TRUE);

		$this->getview->render($this->data);
	}
	
	public function privacy_policy()
	{
		$data = '';

		$this->data['title'] = "TM Engineering Privacy Policy";
		$this->data['page-id'] = "generic-page";
		$this->data['page-styles'] = '';
		$this->data['page-scripts'] = '';

		$this->data['content'] = $this->load->view('pages/privacy-policy', $data, TRUE);

		$this->getview->render($this->data);
	}
	
	public function home_timeline()
	{
		$data = '';

		$products = $this->main_model->find_all_by('product', array('deleted' => '0', 'status' => 'publish'), array('name', 'asc'));
		$this->data['products'] = $products;

		$html = '';

		foreach ($products as $key => $p) {

        $html .= '<a class="cell" href="'.base_url('product/product-detail/'.$p->id.'/'.url_title($p->name, '-',FALSE)).'">
		            <p>'.$p->name.'</p>
		            <img src="'.$this->config->item('admin_url').'/data/images/'.$p->featured_image.'">
		        </a>';
        }

        $content = $this->load->view('pages/home-timeline', $data, TRUE);
        $content = str_replace('{list_product}', $html, $content);

		$this->data['title'] = "Welcome to TM Engineering";
		$this->data['page-id'] = "home-page";
		$this->data['page-styles'] = 'pages/styles/home';
		$this->data['page-scripts'] = 'pages/scripts/home';

		$this->data['content'] = $content;

		$this->getview->render($this->data);
	}
}
