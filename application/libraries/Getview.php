<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Getview {
	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('parser');
		$data = '';
	}

    public function render($data)
    {
    	$this->CI->parser->parse('template/start', $data);
    	$this->CI->parser->parse('template/styles', $data);

    	if ($data['page-styles']) {
    		$this->CI->parser->parse($data['page-styles'], $data);
    	}

    	$this->CI->parser->parse('template/header', $data);
		// $this->CI->parser->parse('theme_sidebar', $data);
		$this->CI->parser->parse('template/content', $data);
		$this->CI->parser->parse('template/footer', $data);
		$this->CI->parser->parse('template/scripts', $data);

		if ($data['page-scripts']) {
    		$this->CI->parser->parse($data['page-scripts'], $data);
    	}

		$this->CI->parser->parse('template/end', $data);
		// $this->load->view('theme_footer');
		// $this->load->view('theme_end');
    }
}
?>