<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{title}</title>
		
		<link data-prerender="keep" rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>/assets/img/favicons/favicon-32x32.png">
		<link data-prerender="keep" rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>/assets/img/favicons/favicon-96x96.png">
		<link data-prerender="keep" rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>/assets/img/favicons/favicon-16x16.png">