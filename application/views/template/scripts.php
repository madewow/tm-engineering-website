<?php
defined('BASEPATH') OR exit('No direct script access allowed');  ?>
		<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/vendor/what-input.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/vendor/foundation.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/app.js"></script>