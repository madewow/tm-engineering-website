 <?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
 	</head>
  
 	<body id="{page-id}">
	 	
 		<div class="off-canvas-wrapper">
	 		<div class="off-canvas position-right" id="offCanvas" data-off-canvas>
		 		<aside>
			 		<ul id="side-navigation">
						<li <?php if($current == ""){ echo 'class="active"'; } ?>>
							<a href="<?php echo base_url() ?>">Home</a>
						</li>

						<li <?php if($current == "about-us"){ echo 'class="active"'; } ?>>
							<a href="<?php echo base_url() ?>about-us">About Us</a>
						</li>

						<li <?php if($current == "product"){ echo 'class="active"'; } ?>>
							<a href="<?php echo base_url() ?>product">Products</a>
							
							<ul>
								<?php foreach ($menu_categories as $c) { ?>
								<li>
									<a href="<?php echo site_url('product'); ?>"><?php echo $c->name; ?></a>
									
									<ul>
										<?php foreach ($c->subcategories as $s) { ?>
										<?php if($current_sub == $s->id) { ?>
										<li class="active">
											<a href="<?php echo site_url('product/subcategory/'.$s->id.'/'.url_title($s->name, '-', TRUE)); ?>"><?php echo $s->name; ?></a>
										</li>
										<?php } else { ?>
										<li>
											<a href="<?php echo site_url('product/subcategory/'.$s->id.'/'.url_title($s->name, '-', TRUE)); ?>"><?php echo $s->name; ?></a>
										</li>
										<?php } ?>
										<?php } ?>
									</ul>

									<div class="trigger">
										<i class="fas fa-chevron-down"></i>
									</div>
								</li>
								<?php } ?>
								
							</ul>
						</li>

						<li <?php if($current == "service-repair"){ echo 'class="active"'; } ?>>
							<a href="<?php echo base_url() ?>service-repair">Service &amp; Repair</a>
						</li>

						<li <?php if($current == "contact"){ echo 'class="active"'; } ?>>
							<a href="<?php echo base_url() ?>contact">Contact</a>
						</li>
					</ul>
		 		</aside>
	 		</div>
	 		
	 		<div class="off-canvas-content" data-off-canvas-content>
		 		<header id="main-header">
		 			<div class="grid-container">
		 				<div class="grid-x">
		 					<div class="cell">
			 					<button class="hide-for-large" data-toggle="offCanvas"><i class="fas fa-bars"></i></button>
			 					
		 						<img src="<?php echo base_url(); ?>assets/img/logo-tm-engineering.png" id="logo">
		
		 						<ul id="main-navigation" class="hide-for-xsmall-only hide-for-small-only hide-for-medium-only hide-for-large-only">
		 							<li <?php if($current == ""){ echo 'class="active"'; } ?>>
		 								<a href="<?php echo base_url() ?>">Home</a>
		 							</li>
		
		 							<li <?php if($current == "about-us"){ echo 'class="active"'; } ?>>
		 								<a href="<?php echo base_url() ?>about-us">About Us</a>
		 							</li>
		
		 							<li <?php if($current == "product"){ echo 'class="active"'; } ?>>
		 								<a href="<?php echo base_url() ?>product">Products</a>
		 								
		 								<ul>
		 									<?php foreach ($menu_categories as $c) { ?>
											<li>
												<a href="<?php echo site_url('product'); ?>"><?php echo $c->name; ?></a>
												
												<ul>
													<?php foreach ($c->subcategories as $s) { ?>
													<?php if($current_sub == $s->id) { ?>
													<li class="active">
														<a href="<?php echo site_url('product/subcategory/'.$s->id.'/'.url_title($s->name, '-', TRUE)); ?>"><?php echo $s->name; ?></a>
													</li>
													<?php } else { ?>
													<li>
														<a href="<?php echo site_url('product/subcategory/'.$s->id.'/'.url_title($s->name, '-', TRUE)); ?>"><?php echo $s->name; ?></a>
													</li>
													<?php } ?>
													<?php } ?>
												</ul>
											</li>
											<?php } ?>
										</ul>
		 							</li>
		
		 							<li <?php if($current == "service-repair"){ echo 'class="active"'; } ?>>
		 								<a href="<?php echo base_url() ?>service-repair">Service &amp; Repair</a>
		 							</li>
		
		 							<li <?php if($current == "contact"){ echo 'class="active"'; } ?>>
		 								<a href="<?php echo base_url() ?>contact">Contact</a>
		 							</li>
		 						</ul>
		 					</div>
		 				</div>
		 			</div>
		 		</header>