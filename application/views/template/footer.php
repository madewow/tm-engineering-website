<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<footer id="main-footer">
	<div class="footer">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell shrink  xsmall-12 medium-4 large-auto xsmall-order-3 large-order-1">
					<address><strong>TM Engineering Ltd.</strong><br>8560 Baxter Place<br>Burnaby, BC<br>V5A 4T2<br>Canada</address>
				</div>
	
				<div class="cell shrink xsmall-12 medium-4 large-auto xsmall-order-1 large-order-2">
					<ul class="footer-navigation">
						<li>
							<a href="<?php echo base_url(); ?>terms-conditions">Terms &amp; Conditions</a>
						</li>
	
						<li>
							<a href="<?php echo base_url(); ?>contact">Contact Us</a>
						</li>
	
						<li>
							<a href="<?php echo base_url(); ?>about-us">About Us</a>
						</li>
	
						<li>
							<a href="<?php echo base_url(); ?>site-map">Site Map</a>
						</li>
					</ul>
				</div>
	
				<div class="cell expand shrink  xsmall-12 medium-8 large-auto xsmall-order-2 large-order-3">
					<ul class="footer-navigation last">
						<li>
							<a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a>
						</li>
	
						<li>
							<a href="mailto:info@tm-engineering.com">info@tm-engineering.com</a>
						</li>
					</ul>
				</div>
	
				<div class="cell xsmall-12 medium-8 large-auto xsmall-order-3">
					<p>Stay up to date on the latest news from TM Engineering</p>
	
					<form id="subscription-form" data-abide novalidate>
						<input type="text" placeholder="Email address" id="email-footer" required>
						
						<p><small>Please enter a valid email address</small></p>
						
						<button class="button"><i class="fas fa-spinner fa-spin"></i><span>Submit</span></button>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="copyright">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell text-center">
					<p>Copyright 2017 <img src="<?php echo base_url(); ?>assets/img/logo-tm-engineering-footer.png" id="logo"> <br>All rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>