<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>

<script>
	$(document).ready(function() {
		$('.hero-image').height($(window).height() - $('#main-header').innerHeight());
	});
    
    $('[rel="open-contact-form"]').click(function(){
        $('#contact-form-accordion').foundation('down', $('#contact-form')); 
    });
    
    $('[rel="close-contact-form"]').click(function(){
        $('#contact-form-accordion').foundation('up', $('#contact-form')); 
    });

	$('#product-image-carousel').slick({
		infinite: false,
		arrows: true,
		fade: true,
		cssEase: 'linear',
		prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-circle-left fa-2x"></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-circle-right fa-2x"></button>'
	});
</script>