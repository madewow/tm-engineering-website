<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
	<header>
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell auto">
                    <h1>Service &amp; Repair</h1>
                </div>
            </div>
        </div>
	</header>

	<div class="hero-image">
	    <div class="grid-container full">
	        <div class="grid-x">
	            <div class="cell xsmall-12 large-7">
		            <img src="<?php echo base_url(); ?>assets/img/header-service-repair.jpg">
	            </div>
	            
	            <div class="quote cell xsmall-12 large-5 grid-x align-center-middle text-center">
		            <blockquote>AT TM ENGINEERING<br>WE DON’T JUST REPAIR AND OFFER YOU PARTS AND LABOUR,<br>WE PROVIDE YOU WITH OVER 40 YEARS OF EXPERIENCE</blockquote>
	            </div>
	        </div>
	    </div>
	</div>


    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell auto">
                <p>With every request for maintenance services, we work out the best maintenance philosophy with your team. Whether you require a full machine overhaul or preventive services, we will work with you to optimise your machines performances and increase your yield.</p>
                
                <h3>REPAIR, OVERHAUL AND MODIFICATION</h3>
                
                <p>We offer both corrective maintenance services for immediate repairs, as well as scheduled preventive maintenance visits for overhauls or modifications.</p>

				<p>We provide a complete overhaul, dismantling your equipment or machinery, repair parts and re-assembling, replacing old parts on equipment or machinery with new parts, troubleshooting mechanical problems during equipment startup, general preventative maintenance.</p>
				
				<h3>INSPECTIONS</h3>
				
				<p>Our inspections are aimed at giving you a detailed report with the technical condition of your equipment. We will advise you with suggested maintenance actions to increase your equipment’s performance and improve your yield.</p>
            </div>
        </div>
    </div>
</section>