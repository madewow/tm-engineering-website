<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
   <header>
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell auto">
                    <h1>{name}</h1>
                </div>
            </div>
        </div>
   </header>
    
    <div class="intro">
	    <div class="grid-container">
		    <div class="grid-x grid-padding-x">
			    <div class="cell xsmall-12 large-auto">
			    {description}

			    </div>
			    
			    <div class="cell xsmall-12 large-auto">
				    <img src="{featured_image}">
			    </div>
		    </div>
	    </div>
    </div>  
</section>

<section>
	<div class="product-list">
	    <div class="grid-container">
            <div class="grid-x grid-padding-x xsmall-up-2 large-up-4">

            	{lists}
                
            </div>
	    </div>
	</div>
</section>