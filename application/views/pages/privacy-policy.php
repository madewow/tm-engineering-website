<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
	<header>
		<div class="grid-container">
	        <div class="grid-x grid-padding-x">
	            <div class="cell auto">
		            <h1>Privacy Policy</h1>
	            </div>
	        </div>
		</div>
	</header>
	
	<div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell auto">
                <p>TM Engineering Ltd. appreciates your interest in our products and our company. Data privacy is important to TM Engineering and we want you to be informed about the information TM Engineering collects and how that data is handled.</p>
                
				<h3>Scope</h3>
				
				<p>This statement applies to TM Engineering Ltd. and the <a href="http://www.tm-engineering.com">www.tm-engineering.com</a> website (“Website”).</p>
				
				<h3>Overview of the Information TM Engineering Collects and How TM Engineering Uses that Information:</h3>
				
				<ul>
					<li>We collect information you choose to submit to fulfill the objective you have requested and for general business management purposes.</li>
					
					<li>That information may include your name, company name, postal address, email address, telephone number, and other identifiers that permit physical or online contact.</li>
					
					<li>If you provide your information to TM Engineering, including name and email address, TM Engineering may send you communications via email containing information about special features featured on the Website or any other service or products TM Engineering thinks may be of interest to you. If you do not wish to receive those communications from TM Engineering, you can unsubscribe at any time by clicking on the link in that email, or by following the instructions in “Your Choices” below.</li>
				</ul>
				
				<h3>Usage Information TM Engineering Gathers:</h3>
					
				<ul>
					<li>TM Engineering may collect information which is based upon your behavior and navigation on the Websites. This information allows TM Engineering to carry out internal research on users’ interests, demographics and behavior so that TM Engineering can better understand, and in turn provide better information, products and services to you and other customers. Please note the information collected only maps IP address and does not contain individual name or identifying information.</li>
					
					<li>"Cookies" are small text files that record internet users’ online activity. Cookies in use on the Website do not collect information that personally identifies a visitor. All information these cookies collect is aggregated and anonymous. This information is only used to improve the performance and design of the website. Certain features on and in the future of this Website are only available if you enable ‘cookies’ in your web browser. You are free to disable the use of cookies although that may impair certain features on the Website. There is further information on how to refuse cookies in “Your Choices” below.</li>
					
					<li>TM Engineering may also use cookies after a user has logged in to a Website.</li>
					
					<li>TM Engineering may also track certain information through the use of JavaScript code and third party website analytics providers. Please note that this does not provide TM Engineering with personal details.</li>
				</ul>
				
				<h3>How TM Engineering Responds to Do Not Track Signals:</h3>
				
				<ul>
					<li>Do Not Track (“DNT”) is a privacy preference that users can set in their web browsers. When a user turns on DNT, the browser sends a message to websites requesting that they don’t track the user.</li>
					
					<li>The ability to use DNT depends on the browser used to access the TM Engineering website. Some latest versions of browsers are able to implement DNT that will be respected by TM Engineering. For more information, click here.</li>
				</ul>
				
				<h3>How TM Engineering Uses Social Plug-Ins:</h3>
				
				<ul>
					<li>TM Engineering uses social plug-ins (also known as “buttons”) of social networks such as Facebook, Google+, and Twitter.</li>
					
					<li>When you visit TM Engineering websites the buttons are not activated, and they will remain inactive unless you click on one of the buttons. After you click on a button, it will remain active until you deactivate them or you delete your cookies.</li>
					
					<li>If you are logged on to a social network, the network can assign your visit to the website to your user account.</li>
					
					<li>If you are a member of a social network, you must log out from the social network before activating the buttons on our website, if you do not want the social network to assign data obtained during your visit to our website with your social membership data.</li>
				</ul>
				
				<h3>When TM Engineering May Transfer Data to Third Parties:</h3>
				
				<ul>
					<li>Where necessary, the information you provided is transmitted to third parties to assist TM Engineering to provide you with products and services as requested by you and maintain the support services provided by TM Engineering. These companies will have access to your personal information as necessary to perform their functions, but they may not use that data for any other purpose.</li>
					
					<li>Subject to the terms and conditions of this Privacy Statement, TM Engineering takes steps to protect all Personal Data from being disclosed to any third parties except when required or permitted by law. For more information on Personal Data, see TM Engineering’s Global Data Privacy Policy, found <a href="http://www.modine.com/web/en/policies.htm">here</a>. In addition, TM Engineering may use your information to assist in the prevention of unlawful activities, in connection with actual or proposed</li>
					
					<li>We may also share your Personal Data with companies in our group and/or our affiliates.</li>
					
					<li>If we or any part of our group is sold, or some of its assets transferred to a third party, your Personal Data may also be transferred to the acquirer, even if they are not in the same line of business as us. Potential purchasers and their advisors may have limited access to Personal Data as part of the sale process. However, use of your Personal Data will remain subject to this Privacy Statement.</li>
					
					<li>Similarly, your Personal Data may be passed on to a successor in interest in the unlikely event of a liquidation, bankruptcy or administration.</li>
				</ul>
				
				<h3>How TM Engineering Protects Personal Data:</h3>
				
				<ul>
					<li>TM Engineering uses technical and organizational security measures designed to try to protect the data you provide us, against manipulation, loss, destruction, and inappropriate access by third parties.</li>
				</ul>
				
				<h3>Your Choices:</h3>
				
				<ul>
					<li>When we collect information from you, you may tell us that you do not want it used for further marketing contact, and we will respect your wishes, by <a href="mailto:privacycontact@na.modine.com?subject=I%20do%20not%20want%20my%20information%20used%20for%20further%20marketing%20contact">clicking this link</a>.</li>
					
					<li>You may turn off cookies in your browser. To find out more about cookies, including how to see what cookies have been set and how to manage and delete them, visit <a href="http://www.aboutcookies.org/">www.aboutcookies.org</a> or <a href="http://www.allaboutcookies.org/">www.allaboutcookies.org</a>. To opt out of being tracked by Google Analytics across all websites visit <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout</a>.</li>
				</ul>
				
				<h3>Updates to Our Privacy Statement:</h3>
				
				<p>We may occasionally post updates to this Privacy Statement. We encourage you to periodically review this page for the latest information on our privacy practices. If you would like to keep a permanent record of this Privacy Statement please print a copy now.</p>
            </div>
        </div>
    </div>
</section>