<?php
	defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section>
	<header>
	    <div class="grid-container">
	        <div class="grid-x grid-padding-x">
	            <div class="cell auto">
	                <h1>Contact TM Engineering</h1>
	            </div>
	        </div>
	    </div>
	</header>

	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-6">
				<form id="contact-form-page" data-abide novalidate>
					<div id="alert-message" class="callout success hide">
                        <p>Your inquiry has been sent.<br>Please allow time for our staff to reply to you.</p>
                    </div>
                    
					<label>
						Name*
						
						<input type="text" name="name" id="name" required>
						
						<p><small>Please enter name</small></p>
					</label>
					
					<label>
						Email*
						
						<input type="email" name="email" id="email" required pattern="email">
						
						<p><small>Please enter a valid email</small></p>
					</label>
					
					<label>
						Details
						
						<textarea name="detail" id="detail" placeholder="Please enter details here, Please be as specific as possible." required></textarea>
						
						<p><small>Please enter your detail</small></p>
					</label>
					
					<div class="grid-x grid-padding-x">
						<div class="cell auto">
							
						</div>
						
						<div class="cell auto shrink">
							<button class="button"><i class="fas fa-spinner fa-spin"></i><span>Submit</span></button>
						</div>
					</div>
				</form>
			</div>
			
			<div class="cell xsmall-12 large-5 xsmall-offset-0 large-offset-1">
				<h4>Global Headquarters</h4>
				
				<p>TM Engineering Ltd.<br>8560 Baxter Place<br>Burnaby, BC<br>V5A 4T2<br>Canada</p>
				
				<p><span>Email</span><br>info@tm-engineering.com</p>
				
				<p><span>Phone</span><br><img src="<?php echo base_url(); ?>/assets/img/icon-flag-canada.gif">&nbsp;&nbsp;604-421-5500<br><img src="<?php echo base_url(); ?>/assets/img/icon-flag-australia.gif">&nbsp;&nbsp;+61(0)421-489-500</p>
			</div>
		</div>
	</div>
</section>