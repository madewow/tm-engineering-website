<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="product-detail">
	<header>
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell text-right">
                   <h1><span><a href="{url_back}">{subtitle}</a></span><br>{name}</h1>
				</div>
			</div>
		</div>
	</header>
	
	<div class="grid-container overview">
		<div class="grid-x grid-padding-x">
			<div class="cell xsmall-12 large-4 xsmall-order-2 large-order-1">
                <h1>{excerpt_title}</h1><p>{excerpt}</p>
                
                <!--<a class="share-product" data-toggle="example-dropdown">
                    <span>
                        <i class="fas fa-share"></i>
                    </span>
                    
                    Share this product
                </a>
                
                <ul class="dropdown-pane" data-position="bottom" data-alignment="left" id="example-dropdown" data-dropdown data-auto-focus="true">
                    <li>
                        <a>
                            <i class="fab fa-slack-hash"></i>
                            Share to Slack
                        </a>
                    </li>
                    
                    <li>
                        <a>
                            <i class="far fa-envelope"></i>
                            Share to email
                        </a>
                    </li>
                    
                    <li>
                        <a>
                            <i class="far fa-copy"></i>
                            Copy url
                        </a>
                    </li>
                </ul>-->
			</div>
			
			<div class="cell xsmall-12 large-8 xsmall-order-1 large-order-2">
				<img src="{featured_image}">
			</div>
		</div>
	</div>
	
	<div class="detail">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell xsmall-12 large-6">
					{content}
				</div>
				
				<div class="cell xsmall-12 large-5 large-offset-1 border">
					<h4>{additional_details}</h4>
					
					<p>
                        {files}
					</p>
					
                    <h4><a rel="open-contact-form">Contact a product specialist</a></h4>
                </div>
			</div>
		</div>
	</div>
</section>

<ul id="contact-form-accordion" class="accordion" data-accordion data-allow-all-closed="true">
    <li class="accordion-item" data-accordion-item>
        <section class="accordion-content" data-tab-content id="contact-form">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell content-box">
                        <a class="button-close" rel="close-contact-form">
                            <i class="fas fa-times-circle fa-2x"></i>
                        </a>
                        
                        <form id="product-specialist-form" class="grid-x grid-padding-x" data-abide novalidate>
	                        <div class="cell xsmall-12">
		                    	<div id="alert-message" class="callout success hide">
									<p>Your product inquiry has been sent. <br>Please allow time for our staff to reply to you.</p>
                    			</div>
	                        </div>
	                        
                            <div class="cell xsmall-12 large-6">
                                <p class="lead">Contact Information</p>

                                <label>First Name*
                                    <input type="text" name="first_name" id="first_name" placeholder="required" required>
                                    
                                    <p><small>Please enter your first name</small></p>
                                </label>

                                <label>Last Name*
                                    <input type="text" name="last_name" id="last_name" placeholder="required" required>
                                    
                                    <p><small>Please enter your last name</small></p>
                                </label>

                                <label>Email*
                                    <input type="email" name="email" id="email" placeholder="required" required pattern="email">
                                    
                                    <p><small>Please enter a valid email address</small></p>
                                </label>

                                <label>Company
                                    <input type="text" id="company">
                                </label>

                                <label>Phone Number*
                                    <input type="text" name="phone" id="phone" placeholder="required" required>
                                    
                                     <p><small>Please enter a valid phone number</small></p>
                                </label>
                            </div>

                            <div class="cell xsmall-12 large-6">
                                <p class="lead">Product Information</p>

                                <label>Purchase Timeline
                                    <select name="purchase_timeline" id="purchase_timeline">
                                        <option value="1 Month">1 Month</option>
                                        <option value="3 Months">3 Months</option>
                                        <option value="6 Months">6 Months</option>
                                        <option value="Unconfirmed">Unconfirmed</option>
                                    </select>
                                </label>

                                <label>Notes/Specific Requirements
                                    <textarea name="detail" id="detail"></textarea>
                                </label>

                                <button class="button"><i class="fas fa-spinner fa-spin"></i><span>Submit</span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </li>
</ul>

<section class="product-image">
	<div class="grid-container">
		<div id="product-image-carousel" class=" carousel grid-x grid-padding-x">
            {images}
		</div>
	</div>
</section>