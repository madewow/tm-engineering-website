<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
	<header>
		<div class="grid-container">
	        <div class="grid-x grid-padding-x">
	            <div class="cell auto">
		            <h1>Terms &amp; Conditions</h1>
	            </div>
	        </div>
		</div>
	</header>
	
	<div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell auto">
                <p>This site is regulated by the terms and conditions specified below. These terms and conditions are binding on the user and as such must be read thoroughly. If you do not intend to accept the terms and conditions, please leave the site. By visiting this site, the user unconditionally accepts the following terms and conditions, including those relating to privacy (see below). TM Engineering Ltd may, at any time and without prior notice, modify these terms and conditions as it sees fit. The user may consequently receive invitations to revisit this web page in order to check any updates. The aim of this site is to entertain, promote and advertise and as such is designed exclusively for private, personal and non-commercial use.</p>
                
                <h3>REGISTERED TRADEMARKS AND COPYRIGHT</h3>
                
                <p>This site contains information, images, photographs, registered trademarks, products, advertising, etc. that are the exclusive property of TM Engineering Ltd. and protected by copyright laws. It is therefore prohibited to reproduce, either fully or in part (other than for private, personal and non-commercial use), publish, transmit, modify, either fully or in part, or sell the material contained in this site. No right, title, or interest shall be transferred as a result of any downloading and/or copying of the material referred to above. In particular, we inform you that the product words and logo are the exclusive property of "TM Engineering Ltd." and that all the trademarks and logos, registered or otherwise, displayed on this site are also the exclusive property of TM Engineering Ltd. Consequently, the reproduction, distribution, publication, modification, wholly or in part, sale or any other use of the above-mentioned material is strictly prohibited. The list reported above is neither official nor final and does not include all the symbols, drawings, models, brands and copyrights owned by TM Engineering Ltd.</p>
                
                <h3>EXCLUSIONS FROM GUARANTEE</h3>
                
				<p>It should be noted that this site could contain errors or incorrect information and as such may be modified or updated by TM Engineering Ltd at any time, at its own discretion without prior notice. Furthermore, it should be remembered that information available on the Internet cannot be considered as 100% “safe” and the site may not function as it should or may contain viruses or dangerous elements. Now and then, this site may display images and scenes with ironic and/or humorous content, which are not intended in any way to cause offence or embarrassment to anyone. If the user does not appreciate the content of the site, he/she is invited to leave it. This site is not designed for children nor does it set out to obtain personal information from minors. If the user has not reached the age of 18, he/she must obtain a parent’s permission to use the site without submitting any personal data.</p>
				
				<h3>RESPONSIBILITY DISCLAIMER</h3>

				<p>TM Engineering Ltd, its employees, directors, managers and shareholders will not lodge complaints or provide guarantees of any type, explicit or implicit, as regards this site or any information, elements, software and any material in general contained herein, explicitly excluding any responsibilities correlated to same. The same cannot be held liable for any for direct or indirect damages, of any type whatsoever, deriving from or relating to the use of this site or the material contained in it. After using the information, elements or material contained in this site, the user assumes full responsibility for any possible losses, costs, expenses and damages, either direct or indirect, of any type or entity, deriving from or linked to the use of this site.</p>
				
				<h3>LINK</h3>
				
				<p>This site may be connected to other sites through links that are not under the control of TM Engineering Ltd. These legal notes are to be considered valid exclusively for the site www.ddtechnologiesinc.com. TM Engineering Ltd, while its employees, directors, managers and shareholders cannot be held liable for content or safety, i.e. for viruses or other negative consequences deriving from the use of the above-mentioned other sites.</p>
 
				<h3>MATERIAL SENT TO TM Engineering Ltd.</h3>
				
				<p>All personal data (name, surname, e-mail address, etc.) supplied to TM Engineering Ltd will be processed in accordance with the latter’s privacy policy. All the information, ideas, images, photographs, music and, in general, all material sent to TM Engineering Ltd in relation to this site will be considered as non-confidential and available to be used freely by TM Engineering Ltd. The transmission of the above information gives TM Engineering Ltd the right to use said information, ideas and materials without any obligation by the company TM Engineering Ltd to confirm receipt, make reference to same, publicise the presentation or provide for any remuneration. The user guarantees that the material sent to TM Engineering Ltd does not violate any third-party rights or any current legislation and that he/she retains responsibility for same and relieves TM Engineering Ltd of any responsibilities relating to claims, damages, losses, costs and expenses, including legal and penal costs deriving from or linked to the material presented or sent.</p>
 
				<h3>COOKIES</h3>
				
				<p>TM Engineering Ltd may use cookies to improve the user’s experience within the site and to provide further information regarding the general use of the site itself. The use of cookies does not signify the collection of personal data by TM Engineering Ltd.</p>
 
				<h3>DISPUTES AND COMPETENT COURT</h3>
				
				<p>In his/her use of the site, the user accepts the jurisdiction of the Italian legal system and the fact that all disputes that may arise relating to the use of the site will be resolved exclusively by the competent Law Courts of British Columbia, Canada.</p>
            </div>
        </div>
    </div>
</section>