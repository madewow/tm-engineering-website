<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section>
	<header>
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell auto">
                    <h1>About TM Engineering</h1>
                </div>
            </div>
        </div>
	</header>

	<div class="hero-image">
	    <div class="grid-container full">
	        <div class="grid-x">
	            <div class="cell large-7">
		            <img src="<?php echo base_url(); ?>assets/img/header-about-us.jpg">
	            </div>
	            
	            <div class="supplier cell large-5">
		            <p class="text-center">We are proud suppliers to Companies such as:</p>
		            
		            <ul class="logos">
						<li>
							<img src="<?php base_url(); ?>assets/img/logo-barrick.png">
						</li>
						
						<li>
							<img src="<?php base_url(); ?>assets/img/logo-newmont.png">
						</li>
							
						<li>
							<img src="<?php base_url(); ?>assets/img/logo-als.png">
						</li>
						
						<li>
							<img src="<?php base_url(); ?>assets/img/logo-codelco.png">
						</li>
							
						<li>
							<img src="<?php base_url(); ?>assets/img/logo-debeers.png">
						</li>
					</ul>
	            </div>
	        </div>
	    </div>
	</div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell auto">
                <p>Founded in 1974, in the heart of Vancouver, British Columbia, Canada, TM Engineering became a leader in analytical laboratory and alluvial mining equipment for grinding, splitting, and rock crushing. Our product line includes equipment such as crushers, pulverisers, sample splitters, drying ovens, riffles, dry blowers and dry concentrators.</p>
                
                <p>From our beginnings in a small shop to now a large production facility, the core principal of TM Engineering will always be to exceed expectations of every customer through service, flexibility, outstanding products, and reliability. We believe in quality, and constant improvement throughout our product line, maximizing the lifespan and value to each machine that leaves our facility. This philosophy has shown through our 44 years of business, through customer repeatability, and exponential growth in sales throughout the company’s history. 
</p>

				<p>Quality is not something to shortchange and neither are customers, this has been embedded in our core team principals since the day the company was founded. Almost everything, right down to the bolts and nuts is produced in house. This keeps all quality control in check to the highest standards. 
</p>

				<p>We commit to provide our customers with the best support for all our machines, old and new, as well as part replacement and technical support. You can find our products in over 150 countries worldwide, and there are no limits to where our products can go. 
</p>

				<p>As TM Engineering increases our product offerings, our manufacturing process is constantly improving with state of the art equipment and high grade materials. Our dedication is maintaining high quality craftsmanship in every product.
</p>
            </div>
        </div>
    </div>
</section>