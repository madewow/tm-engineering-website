<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="hero-image" data-interchange="['<?php echo base_url(); ?>assets/img/hero-image-home01.jpg', xsmall], ['<?php echo base_url(); ?>assets/img/hero-image-home01.jpg', small], ['<?php echo base_url(); ?>assets/img/hero-image-home01.jpg', medium], ['<?php echo base_url(); ?>assets/img/hero-image-home01.jpg', large]">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-right align-middle">
			<div class="cell">
				<h1 class="lead">A world leading<br>manufacturer of sample<br>preparation and alluvial<br>mining systems.</h1>
			</div>
		</div>
	</div>
</section>

<section id="overview">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
			<div class="cell large-11 text-center">
				<p>Providing solutions to assayers for analytical laboratories for over 40 years, we are one of the world's leading  suppliers for lab equipment. </p>

				<p>We also provide mining equipment tailored for alluvial mining in arid climates. With In-house manufacturing; all of our products are designed to meet the highest expectations and built to last through the toughest conditions.</p>
			</div>
		</div>
	</div>
</section>

<section id="product-category">
	<div class="grid-container full">
		<div id="product-category-carousel" class="carousel grid-x">

            {list_product}
          
 		</div>
	</div>
</section>

<section id="map" data-interchange="['<?php echo base_url(); ?>assets/img/home-map.jpg', xsmall], ['<?php echo base_url(); ?>assets/img/home-map.jpg', small], ['<?php echo base_url(); ?>assets/img/home-map.jpg', medium], ['<?php echo base_url(); ?>assets/img/home-map.jpg', large]">
	<div class="grid-container">
		<div class="grid-x align-center-middle">
			<div class="text-center">
				<h2>A GLOBAL LEADER</h2>

				<p>Our products can be found running labs and mines<br>in over 150 countries around the world</p>
			</div>
		</div>
	</div>
</section>

<section id="global-leaders">
	<div class="grid-container fluid">
		<h2 class="lead text-center">TRUSTED BY GLOBAL LEADERS</h2>
		
		<ul class="logos">
			<li>
				<img src="<?php base_url(); ?>assets/img/logo-barrick.png">
			</li>
			
			<li>
				<img src="<?php base_url(); ?>assets/img/logo-newmont.png">
			</li>
				
			<li>
				<img src="<?php base_url(); ?>assets/img/logo-als.png">
			</li>
			
			<li>
				<img src="<?php base_url(); ?>assets/img/logo-codelco.png">
			</li>
				
			<li>
				<img src="<?php base_url(); ?>assets/img/logo-debeers.png">
			</li>
		</ul>
	</div>
</section>

<!--<section id="timeline">
	<div class="grid-container">
		<div class="grid-x grid-padding-x align-right">
			<div class="cell large-6">
				<ul class="accordion" data-accordion data-allow-all-closed="true">
					<li class="accordion-item" data-accordion-item>
				    	<a href="#" class="accordion-title">1977</a>
				    
				    	<div class="accordion-content" data-tab-content>
				      		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				    	</div>
				  	</li>

				  	<li class="accordion-item" data-accordion-item>
				  		<a href="#" class="accordion-title">1982</a>
				    
				    	<div class="accordion-content" data-tab-content>
				      		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				    	</div>
				  	</li>

				  	<li class="accordion-item" data-accordion-item>
				    	<a href="#" class="accordion-title">1998</a>
				    
				    	<div class="accordion-content" data-tab-content>
				      		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				    	</div>
				  	</li>

				  	<li class="accordion-item" data-accordion-item>
				    	<a href="#" class="accordion-title">2002</a>
				    
				    	<div class="accordion-content" data-tab-content>
				      		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				    	</div>
				  	</li>
				</ul>
			</div>
		</div>
	</div>
</section>-->

<section id="commitment">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell content-box">
				<div class="grid-x grid-padding-x">
					<div class="cell">
						<h2 class="text-center">Our Commitment</h2>
					</div>
				</div>

				<div class="grid-x grid-padding-x">
					<div class="cell xsmall-12 text-center">
						<p>From our beginnings in a small shop to now a large production facility, the core principal of TM Engineering will always be to exceed expectations of every customer through service, flexibility, outstanding products, and reliability. We believe in quality, and constant improvement throughout our product line, maximizing the lifespan and value to each machine that leaves our facility. This philosophy has shown through our 44 years of business, through customer repeatability, and exponential growth in sales throughout the company’s history.</p>

						<p>Quality is not something to shortchange and neither are customers, this has been embedded in our core team principals since the day the company was founded. Almost everything, right down to the bolts and nuts is produced in house. This keeps all quality control in check to the highest standards. </p>
						
						<p>We commit to provide our customers with the best support for all our machines, old and new, as well as part replacement and technical support. You can find our products in over 150 countries worldwide, and there are no limits to where our products can go.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>