<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php foreach ($categories as $key => $c) { ?>

<section>
    <header>
         <div class="title-bar">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h2><?php echo $c->name; ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php if(empty($categories[$key]->subs)) { ?>
    <div class="product-list">
        <div class="grid-container">
            empty list
        </div>
    </div>
    <?php } ?>

    <div class="product-list">
        <div class="grid-container">
            <div class="grid-x grid-padding-x xsmall-up-2 medium-up-3 large-up-4">
                <?php foreach ($categories[$key]->subs as $key_sub => $s) { ?>
                <a class="cell" href="<?php echo base_url(); ?>product/subcategory/<?php echo $s->id; ?>">
                    <p><?php echo $s->name; ?></p>
                    <?php if(file_exists(FCPATH.'/admin/data/images/'.$s->featured_image)) { ?>
                    <img src="<?php echo $this->config->item('admin_url').'/data/images/'.$s->featured_image; ?>">
                    <?php } else { ?>
                    <img src="<?php echo base_url('assets/img/no_image.jpg'); ?>">
                    <?php } ?>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>

</section>

<?php } ?>