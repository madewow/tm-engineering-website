<!-- BEGIN MINI-PROFILE -->
      <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        
        <!-- END MINI-PROFILE -->

        <!-- BEGIN SIDEBAR MENU -->
        <p class="menu-title sm">MENU <span class="pull-right"><a href="javascript:;"></p>
        <ul>
          <li>
            <a href="<?php echo site_url('search'); ?>"> <i class="fa fa-search"></i> <span class="title">Search</span> </a>
          </li>
          <li>
            <a href="<?php echo site_url('categories'); ?>"> <i class="fa fa-book"></i> <span class="title">Categories</span> </a>
            <ul class="sub-menu">
              <li> <a href="<?php echo site_url('categories'); ?>"> Categories </a></li>
              <li> <a href="<?php echo site_url('subcategories'); ?>"> Sub-Categories </a></li>
            </ul>
          </li>
          <li>
            <a href="<?php echo site_url('products'); ?>"> <i class="fa fa-cube"></i> <span class="title">Products</span> </a>

            <ul class="sub-menu">

              <?php foreach ($header_categories as $c) { ?>
                <li> 
                    <a href="<?php echo site_url('products?filter='.$c->name); ?>"> <?php echo $c->name; ?> </a>
                    <?php if (!empty($c->subcategories)) { ?>
                    <ul class="sub-menu">
                      <?php foreach ($c->subcategories as $s) { ?>
                      <li> <a href="<?php echo site_url('products?filter='.$s->sub_name); ?>"> <?php echo $s->sub_name; ?>  </a></li>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
              <?php } ?>

            </ul>

          </li>
          <li>
            <a href="<?php echo site_url('authors'); ?>"> <i class="fa fa-user"></i> <span class="title">Author</span> </a>
          </li>
           <li>
            <a href="<?php echo site_url('forms/contact'); ?>"> <i class="fa fa-envelope"></i> <span class="title">Contact Form</span> </a>
          </li>
          <li>
            <a href="<?php echo site_url('forms/specialist'); ?>"> <i class="fa fa-cog"></i> <span class="title">Specialist Request Form</span> </a>
          </li>
          <li>
            <a href="<?php echo site_url('forms/subscriber'); ?>"> <i class="fa fa-envelope-o"></i> <span class="title">Subscriber</span> </a>
          </li>
        </ul>
        </a>
        </li>
        </ul>
        
        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
      </div>