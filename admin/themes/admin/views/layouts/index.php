<!DOCTYPE html>
<html>

<head>
  <title>
     Product Management</title>

  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <!-- <link rel="icon" href="<?php echo $this->template->get_theme_path();?>images/cropped-Salad-Circus-Logo-XS.png" sizes="192x192" /> -->

  <!-- BEGIN PLUGIN CSS -->
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-datatable/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/ios-switch/ios7-switch.css" rel="stylesheet" type="text/css" media="screen">

  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-notifications/css/messenger.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-notifications/css/messenger-theme-flat.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- END PLUGIN CSS -->

  <!-- BEGIN PLUGIN CSS -->
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/animate.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />

  <!-- END PLUGIN CSS -->

  <!-- BEGIN CORE CSS FRAMEWORK -->
  <link href="<?php echo $this->template->get_theme_path();?>webarch/css/webarch.css" rel="stylesheet" type="text/css" />
  <!-- END CORE CSS FRAMEWORK -->

  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>


  <!-- easy autocomplete -->
  <link rel="stylesheet" href="<?php echo $this->template->get_theme_path();?>css/easy-autocomplete.min.css">
  <link rel="stylesheet" href="<?php echo $this->template->get_theme_path();?>css/easy-autocomplete.themes.min.css"> 
  <script src="<?php echo $this->template->get_theme_path();?>js/jquery.easy-autocomplete.min.js"></script> 



</head>

<style>

  .title-app{
      font-size: 22px;
      margin-top: 20px;
      margin-left: 23px;
      color:#ffffff;
  }

  .header-seperation {
    background-color: #0aa699;
  }

  .page-sidebar.mini {
    
    background-color: #0aa699 !important;
    
  }

  .pointer{
    cursor:pointer !important;
  }

  input[type=number]::-webkit-inner-spin-button, 
  input[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
    margin: 0; 
  }

  .m-b-none {
    margin-bottom: 0;
  }

  .m-t-none {
    margin-top: 0;
  }

  .pagination {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
  }

  .pagination>li>a, .pagination>li>span {
    
    color: #5e5e5e;
    
  }

  .pagination-sm>li>a, .pagination-sm>li>span {
    padding: 9px 11px;
    font-size: 12px;
    line-height: 1.5;
  }

  .header-top{

    margin-left: 0px !important;
    margin-top: 0px !important;
    margin-right: 0px !important;
    height: 60px !important;
  }

  .header-top >li > a{

    height : 60px !important;
  }


</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="">
  <!-- BEGIN HEADER -->
  <div class="header navbar navbar-inverse ">



    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
      <div class="header-seperation">
        <ul class="nav pull-left notifcation-center visible-xs visible-sm">
          <li class="dropdown">
            <a href="#main-menu" data-webarch="toggle-left-side">
                <i class="material-icons">menu</i>
              </a>
          </li>
        </ul>
        <!-- BEGIN LOGO -->
        <a>
          <h4 class="title-app">Product Management</h4>
        </a>
        <!-- END LOGO -->
        <!-- <ul class="nav pull-right notifcation-center">
          <li class="dropdown hidden-xs hidden-sm">
            <a href="index.html" class="dropdown-toggle active" data-toggle="">
                <i class="material-icons">home</i>
              </a>
          </li>
        </ul> -->
      </div>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <div class="header-quick-nav">
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="pull-left">
          <ul class="nav quick-section">
            <li class="quicklinks">
              <a href="#" class="" id="layout-condensed-toggle">
                  <i class="material-icons">menu</i>
                </a>
            </li>
          </ul>
          <ul class="nav quick-section">
            
            
            <li class="quicklinks"> <span class="h-seperate"></span></li>
            <!-- <li class="quicklinks">
              <a href="#" class="" id="my-task-list" data-placement="bottom" data-content='' data-toggle="dropdown" data-original-title="Notifications">
                  <i class="material-icons">notifications_none</i>
                  <span class="badge badge-important bubble-only"></span>
                </a>
            </li> -->
            
            
          </ul>
          
        </div>

        

        <!-- BEGIN CHAT TOGGLER -->
        <div class="pull-right">
         
          <ul class="nav quick-section ">
              <li class="quicklinks">
                
                <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">
                  <i class="material-icons">tune</i>
                </a>
                <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                  <li>
                    <a href="<?php echo site_url('authors/change_password') ?>"> Change Password</a>
                  </li>
                  
                  <li class="divider"></li>
                  <li>
                    <a href="<?php echo site_url('logout') ?>"><i class="material-icons">power_settings_new</i>&nbsp;&nbsp;Log Out</a>
                  </li>
                </ul>
              </li>
              
            </ul>
        </div>
        <!-- END CHAT TOGGLER -->
      </div>
      <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
  </div>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar " id="main-menu">

      <!-- BEGIN MINI-PROFILE -->
      <!-- <?php echo $this->template->set_sidebar['sidebar_admin']; ?> -->
      <?php echo $template['partials']['sidebar']; ?>
    </div>
    <a href="#" class="scrollup">Scroll</a>
    <!-- <div class="footer-widget">
      <div class="pull-right">
        <a href="lockscreen.html"><i class="material-icons">power_settings_new</i></a></div>
    </div> -->
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      <div class="clearfix"></div>
      <?php echo $template['body']; ?>

    </div>
  </div>
  
  
  <!-- END CONTAINER -->
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
  <!-- BEGIN JS DEPENDECENCIES-->
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js"
    type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
 
  <!-- END CORE JS DEPENDECENCIES-->
  <!-- BEGIN CORE TEMPLATE JS -->
  <script src="<?php echo $this->template->get_theme_path();?>webarch/js/webarch.js" type="text/javascript"></script>
  <!-- END CORE TEMPLATE JS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"
    type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
    type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
   <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-datatable/js/jquery.dataTables.js"
    type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-datatable/extra/js/dataTables.tableTools.min.js"
    type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path();?>assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
  <script type="text/javascript" src="<?php echo $this->template->get_theme_path();?>assets/plugins/datatables-responsive/js/lodash.min.js"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/js/datatables.js" type="text/javascript"></script> 
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
  
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/ios-switch/ios7-switch.js" type="text/javascript"></script>
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS INIT -->
  
  <script src="<?php echo $this->template->get_theme_path();?>assets/js/validator.js" type="text/javascript"></script>
  <!-- END JAVASCRIPTS -->
  <script src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-notifications/js/messenger.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo $this->template->get_theme_path();?>assets/plugins/jquery-notifications/js/demo/demo.js"></script>
  <script type="text/javascript" src="<?php echo $this->template->get_theme_path();?>assets/js/notifications.js"></script>

 
 
   <script src="<?php echo $this->template->get_theme_path();?>assets/js/form_elements.js" type="text/javascript"></script>
  
</body>

</html>