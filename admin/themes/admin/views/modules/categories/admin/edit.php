<div class="content">
          <ul class="breadcrumb">
            <li>
              <p><?php echo $title ?></p>
            </li>
            <li><a href="#" class="active"><?php echo $subtitle ?></a>
            </li>
          </ul>
          <div class="page-title"> 
            <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
            
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <!-- -->
                </div>
                <div class="grid-body no-border">
                  <?php echo print_message_admin($this->session->flashdata('message')); ?>
                  <form action="" id="form_traditional_validation" name="form_traditional_validation" role="form" autocomplete="off" class="validate" method="post">
                    <div class="form-group">
                      <label class="form-label">Category Name</label>
                      <input class="form-control" id="name" name="name" value="<?php echo $data->name; ?>" type="text" required>
                    </div>
                    <!--<div class="form-group">
                      <label class="form-label">Parent Category</label>
                      <input class="form-control" id="form1Amount" name="parent" type="text" required>
                    </div>-->
                    
                    <!--<div class="form-group">
                      <label class="form-label">Featured Product</label>
                      <input class="form-control" id="form1Amount" name="featured" type="text" required>
                    </div>-->
                    
                    
                    <div class="form-actions">
                      <div class="pull-right">
                        <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                        <a href="<?php echo site_url('categories') ?>"><button class="btn btn-white btn-cons" type="button">Back</button></a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
          

        
        <!-- END PAGE -->