<div class="content">
          <ul class="breadcrumb">
            <li>
              <p><?php echo $title ?></p>
            </li>
            <li><a href="#" class="active"><?php echo $subtitle ?></a>
            </li>
          </ul>
          <div class="page-title"> 
            <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
            <div class="pull-right actions">
              <a class="pointer" href="<?php echo site_url('categories/add') ?>"><button class="btn btn-primary btn-cons" type="button">Add</button></a>
             
            </div>
          </div>
          <?php echo print_message_admin($this->session->flashdata('message')); ?>
          <div class="row">
            <div class="col-md-12">
              <div class="grid simple ">
                <div class="grid-title no-border">
                  <!-- <h4>Table  <span class="semi-bold">Styles</span></h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#grid-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                  </div> -->
                </div>
                <div class="grid-body no-border">
                  <!-- <h3>Basic  <span class="semi-bold">Table</span></h3> -->
                  <table class="table table-hover table-condensed" id="example">
                    <thead>

                      <tr>
                        <th >Name</th>
                        
                        <th >Date Created</th>
                        
                        <th style="width:10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(!empty($lists)){
                          foreach($lists as $l){ ?>
                      <tr>
                        
                        <td class="v-align-middle"><?php echo $l->name ?></td>
                        
                        <td class="v-align-middle">
                          <?php echo human_datetime($l->created_on) ?>
                          
                        </td>
                        
                        <td class="v-align-middle">
                          <a href="<?php echo site_url('categories/edit/'.$l->id) ?>" class="pointer">
                            <i class="material-icons" title="edit">border_color</i>
                          </a>
                          <a href="<?php echo site_url('categories/delete/'.$l->id) ?>" class="pointer" onclick="return confirmation()">
                            <i class="material-icons" title="delete">delete</i>
                          </a>
                        </td>
                      </tr>
                      <?php }} ?>
                      
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 text-left"> </div>
            <div class="col-sm-3 text-center"></div>
            <div class="col-sm-5 text-right text-center-xs">
                  <ul class="pagination pagination-sm m-t-none m-b-none">
                      
                    <?php echo $this->pagination->create_links(); ?>
                  </ul>
            </div>
          </div>
        </div>

        <script type="text/javascript">
            function confirmation() {
          
            if (confirm("Are you sure want to delete this item") == true) {
                return true;
            } else {
                return false;
            }
            
        }
        </script>
        <!-- END PAGE -->
