<script src="<?php echo $this->template->get_theme_path();?>js/chained/chained.min.js"></script>
<!--CKEDITOR-->
<script src="https://cdn.ckeditor.com/4.9.0/standard/ckeditor.js"></script>
<div class="content">
          <ul class="breadcrumb">
            <li>
              <p><?php echo $title ?></p>
            </li>
            <li><a href="#" class="active"><?php echo $subtitle ?></a>
            </li>
          </ul>
          <div class="page-title"> 
            <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
            
          </div>
          <?php echo print_message_admin($this->session->flashdata('message')); ?>
          <div class="row">
            <div class="col-md-8">
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <!-- -->
                </div>
                <div class="grid-body no-border">
                  <form action="" id="form_traditional_validation" name="form_traditional_validation" role="form" autocomplete="off" class="validate" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-12">
                      <label class="form-label">Product Name</label>
                      <input class="form-control" id="form1Amount" name="name" type="text" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="form-label">Category</label>
                      <select class="form-control" id="category" name="category_id" type="text" required>
                        <option value="">Choose Category</option>
                        <?php foreach ($categories as $c) { ?>
                        <option value="<?php echo $c->id?>"><?php echo $c->name?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="form-label">Category Sub</label>
                      <select class="form-control" id="category_sub" name="category_sub_id">
                        <option value="">Choose Category Sub</option>
                        <?php foreach ($subcategories as $s) { ?>
                        <option class="<?php echo $s->category_id?>" value="<?php echo $s->id?>"><?php echo $s->sub_name?></option>
                        <?php } ?>
                      </select>
                    </div>
                    
                    <div class="form-group col-md-12">
                      <textarea id="editor" name="content" class="form-control" rows="20"></textarea>
                    </div>

                    <div class="form-group col-md-12">
                      <label class="form-label">Excerpt Title</label>
                      <input class="form-control" name="excerpt_title" type="text" required>
                    </div>

                    <div class="form-group col-md-12">
                      <label class="form-label">Excerpt</label>
                      <textarea class="form-control" name="excerpt" rows="3"></textarea>
                    </div>

                    <div class="form-group col-md-12">
                      <label class="form-label">Product File Upload (PDF,DOC,XLS,PPT,JPG)</label>
                      <div class="col-md-12"><input type="file" name="product_file[]" class="form-control"></div>
                      <div id="product_file"></div><br><br>
                      <div class="col-md-12"><a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="add_file()"><i class="fa fa-plus"></i> Add file</a></div>
                    </div>

                    <div class="form-group col-md-12">
                      <label class="form-label">Featured Image</label>
                      <input type="file" name="featured_image" class="form-control"/>
                    </div>

                    <div class="form-group col-md-12">
                      <label class="form-label">Additional Product Images</label>
                      <div class="col-md-12"><input type="file" name="product_image[]" class="form-control"></div>
                      <div id="product_image"></div><br><br>
                      <div class="col-md-12"><a href="javascript:void(0)" class="btn btn-small btn-primary" onclick="add_image()"><i class="fa fa-plus"></i> Add Image</a></div>
                    </div>
                    
                    <div class="form-group col-md-12">
                      <label class="form-label">SEO</label>
                      <textarea class="form-control" name="meta_description"></textarea>
                    </div>
                    
                    <div class="form-actions">
                      <div class="pull-right col-md-12">
                        <!-- <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i>Save Draft</button> -->
                        <input type="submit" name="status" value="draft" class="btn btn-cons btn-white">
                        <input type="submit" name="status" value="publish" class="btn btn-cons btn-white">
                        <input type="submit" name="status" value="unpublish" class="btn btn-cons btn-white">
                        <!-- 
                        <a href="<?php echo site_url('categories') ?>"><button class="btn btn-white btn-cons" type="button">Publish</button></a>
                        <a href="<?php echo site_url('categories') ?>"><button class="btn btn-white btn-cons" type="button">UnPublish</button></a> -->
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
        
        <!-- END PAGE -->
        <script type="text/javascript">
          $(document).ready(function(){
            $("#category_sub").chained("#category");
            CKEDITOR.replace( 'editor' );
          });

          var product_file = 1;
          function add_file()
          {
            $("#product_file").append('<div id="container_'+product_file+'"><br/><br/><div class="col-md-11"><input class="form-control" id="product_file_'+product_file+'" type="file" name="product_file[]"></div><div class="col-md-1"><a href="javascript:void(0)" onclick="remove_file(\''+product_file+'\')"><i class="fa fa-close"></i></a></div></div>');
            product_file++;
          }

          function remove_file(id)
          {
            $("#container_"+id).html('');
            $("#container_"+id).remove();
          }

          var product_image = 1;
          function add_image()
          {
            $("#product_image").append('<div id="container_image'+product_image+'"><br/><br/><div class="col-md-11"><input class="form-control" id="product_image_'+product_image+'" type="file" name="product_image[]"></div><div class="col-md-1"><a href="javascript:void(0)" onclick="remove_image(\''+product_image+'\')"><i class="fa fa-close"></i></a></div></div>');
            product_image++;
          }

          function remove_image(id)
          {
            $("#container_image"+id).html('');
            $("#container_image"+id).remove();
          }
        </script>