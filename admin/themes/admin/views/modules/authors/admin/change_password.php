<div class="content">
          <ul class="breadcrumb">
            <li>
              <p><?php echo $title ?></p>
            </li>
            <li><a href="#" class="active"><?php echo $subtitle ?></a>
            </li>
          </ul>
          <div class="page-title"> 
            <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
            
          </div>
          <?php echo print_message_admin($this->session->flashdata('message')); ?>
          <div class="row">
            <div class="col-md-6">
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <!-- -->
                </div>
                <div class="grid-body no-border">
                  <form action="<?php echo site_url('authors/update_password') ?>" method="post" data-toggle="validator" role="form"> 
                    <div class="form-group">
                      <label class="form-label">Current Password</label>
                      <input class="form-control" id="curr_password" name="curr_pass" type="password" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <label class="form-label">New Password</label>
                      <input class="form-control" id="new_password" name="new_pass" type="password" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <label class="form-label">New Confirmed Password</label>
                      <input class="form-control" id="conf_password" name="new_conf_pass" type="password" data-match="#new_password" data-match-error="password didnt match" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    
                    
                    
                    <div class="form-actions">
                      <div class="pull-right">
                        <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                        <a href="<?php echo site_url('authors') ?>"><button class="btn btn-white btn-cons" type="button">Cancel</button></a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
          

        
        <!-- END PAGE -->