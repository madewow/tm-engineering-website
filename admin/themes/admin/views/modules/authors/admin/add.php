<div class="content">
          <ul class="breadcrumb">
            <li>
              <p><?php echo $title ?></p>
            </li>
            <li><a href="#" class="active"><?php echo $subtitle ?></a>
            </li>
          </ul>
          <div class="page-title"> 
            <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
            
          </div>
          <?php echo print_message_admin($this->session->flashdata('message')); ?>
          <div class="row">
            <div class="col-md-6">
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <!-- -->
                </div>
                <div class="grid-body no-border">
                  <form action="<?php echo site_url('authors/store') ?>" method="post">
                    <div class="form-group">
                      <label class="form-label">Name</label>
                      <input class="form-control" id="form1Amount" name="name" type="text" required>
                    </div>
                    <div class="form-group">
                      <label class="form-label">Email</label>
                      <input class="form-control" id="form1Amount" name="email" type="email" required>
                    </div>
                    <div class="form-group">
                      <label class="form-label">Password</label>
                      <input class="form-control" id="form1Amount" name="password" type="password" required>
                    </div>
                    <div class="form-group">
                      <label class="form-label">Confirmed Password</label>
                      <input class="form-control" id="form1Amount" name="conf_password" type="password" required>
                    </div>
                    
                    
                    <div class="form-actions">
                      <div class="pull-right">
                        <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                        <a href="<?php echo site_url('admin/user') ?>"><button class="btn btn-white btn-cons" type="button">Cancel</button></a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
          </div>
          

        
        <!-- END PAGE -->