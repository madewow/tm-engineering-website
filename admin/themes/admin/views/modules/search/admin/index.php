<script src="<?php echo $this->template->get_theme_path();?>js/chained/chained.min.js"></script>
<div class="content">
	<ul class="breadcrumb">
		<li>
		  <p><?php echo $title ?></p>
		</li>
		<li><a href="#" class="active"><?php echo $subtitle ?></a>
		</li>
	</ul>
    <div class="page-title"> 
        <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
        <div class="pull-right actions">
          	
        </div>
    </div>
    <?php echo print_message_admin($this->session->flashdata('message')); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  
                </div>

                <div class="grid-body no-border">
                    <div class="form-group">
                        <label class="form-label">Category Name</label>
                        <select class="form-control" id="category" name="category" onchange="find_products()">
                            <option value="">Choose Category</option>
                            <?php foreach ($categories as $c) { ?>
                            <option value="<?php echo $c->id?>"><?php echo $c->name?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Sub Category Name</label>
                        <select class="form-control" id="sub_category" name="sub_category" onchange="find_products()">
                            <option value="">Choose Sub Category</option>
                            <?php foreach ($subcategories as $s) { ?>
                            <option class="<?php echo $s->category_id?>" value="<?php echo $s->id?>"><?php echo $s->sub_name?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        Products: 
                        <ul>
                            <div id="list_product">
                                
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
    	</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#sub_category").chained("#category");
});

function find_products(){

    $("#list_product").html('loading');

    var category        = $("#category").val();
    var sub_category    = $("#sub_category").val();

    $.post( "<?php echo site_url()?>search/api/products?key=d9d9b483f3b94c962f64424ce03d84ae0d330b0b", { 
        id_category: category, 
        id_subcategory: sub_category 
    })
    .done(function( data ) {
        console.log(data.data)
        if(data.data.length == 1)
        {
            window.location.href = "<?php echo site_url('products/edit'); ?>/"+data.data[0].id;
        }
        else
        {
            var html = '';
            for (var i = 0; i < data.data.length; i++) {
                html += "<li><a href='<?php echo site_url('products/edit'); ?>/"+data.data[i].id+"'>"+data.data[i].name+"</a></li>";
            }

            $("#list_product").html(html);
        }
    });

}
</script>