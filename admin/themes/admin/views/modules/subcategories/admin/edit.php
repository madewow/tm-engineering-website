<script src="https://cdn.ckeditor.com/4.9.0/standard/ckeditor.js"></script>

<div class="content">
<ul class="breadcrumb">
  <li>
    <p><?php echo $title ?></p>
  </li>
  <li><a href="#" class="active"><?php echo $subtitle ?></a>
  </li>
</ul>
<div class="page-title"> 
  <h3><span class="semi-bold"><?php echo strtoupper($title); ?></span></h3>
  
</div>
<div class="row">
  <div class="col-md-6">
    <div class="grid simple form-grid">
      <div class="grid-title no-border">
        <!-- -->
      </div>
      <div class="grid-body no-border">
        <?php echo print_message_admin($this->session->flashdata('message')); ?>
        <form action="" id="form_traditional_validation" name="form_traditional_validation" role="form" autocomplete="off" class="validate" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label class="form-label">Category</label>
            <select class="form-control" id="category_id" name="category_id" required>
              <option value="">Choose Category</option>
              <?php foreach ($categories as $c) { if($c->id == $data->category_id){ $select = "selected"; }else{ $select = ""; } ?>
              <option value="<?php echo $c->id?>" <?php echo $select; ?>><?php echo $c->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label class="form-label">Sub Category Name</label>
            <input class="form-control" id="name" name="name" value="<?php echo $data->name; ?>" type="text" required>
          </div>
          <div class="form-group">
            <label class="form-label">Description</label>
            <textarea class="form-control" id="editor" name="description" rows="15"><?php echo $data->description; ?></textarea>
          </div>
          <div class="form-group">
            <label class="form-label">Featured Image</label>
            <input type="file" name="featured_image" class="form-control"/> 
            <?php if($data->featured_image) { ?>
            <img src="<?php echo site_url('data/images/'.$data->featured_image); ?>" width="100"><br/><br/>
            <?php } ?>
          </div>
          
          
          <div class="form-actions">
            <div class="pull-right">
              <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
              <a href="<?php echo site_url('subcategories') ?>"><button class="btn btn-white btn-cons" type="button">Back</button></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
</div>
          
<script type="text/javascript">
  $(document).ready(function(){
    CKEDITOR.replace( 'editor' );
  });
</script>
        
        <!-- END PAGE -->

  
<br>
<br>
<br>
<br>