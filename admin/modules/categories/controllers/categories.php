<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class categories extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('categories/category_model', 'category');

        $this->check_login();
    }

    public function index(){
        $lists = $this->category->where('deleted', '0');
        $lists = $this->category->find_all();

    	$this->template->set('title', 'Category')
                        ->set('subtitle', 'List Category')
                        ->set('lists', $lists)
                        ->build('admin/index');
    }

    public function add(){

        if($this->input->post())
        {
            $data = array(
                'name' => $this->input->post('name') 
            );

            $category_id = $this->category->insert($data);

            if($category_id)
            {
                $this->session->set_flashdata('message', 'success|insert category success');
                redirect(site_url('categories'), 'refresh');
            }
        }

    	$this->template->set('title', 'Category')
                        ->set('subtitle', 'Create Category')
                        ->build('admin/add');
    }

    public function edit($id)
    {
        if($this->input->post())
        {
            $data = array(
                'name' => $this->input->post('name') 
            );

            $category_update = $this->category->update($id, $data);

            if($category_update)
            {
                $this->session->set_flashdata('message', 'success|update category success');
                redirect(site_url('categories/edit/'.$id), 'refresh');
            }
        }
           
        $data = $this->category->find($id);

        $this->template->set('title', 'Category')
                        ->set('subtitle', 'Edit Category')
                        ->set('data', $data)
                        ->build('admin/edit');
    }

    public function delete($id)
    {
        $category_delete = $this->category->set_soft_deletes(TRUE);
        $category_delete = $this->category->delete($id);

        if($category_delete)
        {
            $this->session->set_flashdata('message', 'success|delete category success');
            redirect(site_url('categories'), 'refresh');
        }
    }
}

?>