<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Api extends Api_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('products/product_model', 'product');
    }

	public function products()
    {
        $id_category    = $this->input->post('id_category');
        $id_subcategory = $this->input->post('id_subcategory');

        if(!empty($id_category) && !empty($id_subcategory))
        {
            $keyword = array(
                'product.category_id'       => $id_category,
                'product.category_sub_id'   => $id_subcategory
            );
        }
        else
        {
            $keyword = array(
                'product.category_id'       => $id_category
            );
        }

        $data = $this->product->find_all_by($keyword);

        $this->rest->set_data($data);
        $this->rest->render();
    }

}