<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class search extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('products/product_model', 'product');
        $this->load->model('products/product_file_model', 'product_file');
        $this->load->model('products/product_image_model', 'product_image');
        $this->load->model('categories/category_model', 'category');
        $this->load->model('subcategories/subcategory_model', 'subcategory');

        $this->check_login();
    }

    public function index(){
        $categories     = $this->category->find_all();
        $subcategories  = $this->subcategory->find_all();

    	$this->template->set('title', 'Search')
                        // ->set('subtitle', 'Search')
                        // ->set('lists', $lists)
                        ->set('categories', $categories)
                        ->set('subcategories', $subcategories)
                        ->build('admin/index');
    }
}