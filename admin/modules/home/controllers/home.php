<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home extends Front_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('home/home_model', 'home');
    }

    public function index(){
    	$this->template->build('index');
    }

    
}

?>