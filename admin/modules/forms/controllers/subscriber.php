<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class subscriber extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('api/subscription_form_model', 'subscriber_form');

        $this->check_login();
        
    }

    public function index(){

        $lists = $this->subscriber_form->order_by('id', 'desc');
        $lists = $this->subscriber_form->find_all_by(array('deleted' => '0'));

    	$this->template
                    ->set('lists',$lists)
                    ->build('admin/subscriber');
    }

    public function download()
    {
    	$this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');

        date_default_timezone_set("Asia/Jakarta");

        $filename  = "Export (".date('d-m-Y').").csv";
        $query     = "SELECT * ".
                     "FROM subscription_form";
        $delimiter = ",";
        $newline   = "\r\n";

        $result    = $this->db->query($query);
        $csv_file  = $this->dbutil->csv_from_result($result, $delimiter, $newline);

        force_download($filename, $csv_file);
    }

}