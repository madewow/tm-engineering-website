<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class contact extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('api/contact_form_model', 'contact_form');

        $this->check_login();
        
    }

    public function index(){

        $lists = $this->contact_form->order_by('id', 'desc');
        $lists = $this->contact_form->find_all_by(array('deleted' => '0'));

    	$this->template
                    ->set('lists',$lists)
                    ->build('admin/contact');
    }

}