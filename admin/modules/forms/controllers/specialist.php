<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class specialist extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('api/specialist_form_model', 'specialist_form');

        $this->check_login();
        
    }

    public function index(){

        $lists = $this->specialist_form->order_by('id', 'desc');
        $lists = $this->specialist_form->find_all_by(array('deleted' => '0'));

    	$this->template
                    ->set('lists',$lists)
                    ->build('admin/specialist');
    }

}