<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class subcategory_model extends MY_Model {

	protected $table        = 'category_sub';
	protected $table_cat    = 'category';
    protected $key          = 'id';
    protected $date_format  = 'datetime';
    protected $set_created  = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function find_all()
    {
    	return $this->db->select($this->table.'.id, '.$this->table_cat.'.name, '.$this->table.'.name as sub_name, '.$this->table.'.created_on')
                        ->select($this->table.'.category_id')
    					->from($this->table)
    					->join($this->table_cat, $this->table_cat.'.id = '.$this->table.'.category_id')
    					->where($this->table.'.deleted', '0')
    					->where($this->table_cat.'.deleted', '0')
    					->get()
    					->result();
    }

}
