<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class subcategories extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('categories/category_model', 'category');
        $this->load->model('subcategories/subcategory_model', 'subcategory');

        $this->check_login();
    }

    public function index(){
        $lists = $this->subcategory->find_all();

    	$this->template->set('title', 'Sub Category')
                        ->set('subtitle', 'List Sub Category')
                        ->set('lists', $lists)
                        ->build('admin/index');
    }

    public function add(){

        if($this->input->post())
        {
            $data = array(
                'category_id' 	=> $this->input->post('category_id'),
                'name'          => $this->input->post('name'), 
                'description' 	=> $this->input->post('description') 
            );

            $config['allowed_types']        = '*';
            $config['max_size']             = 1024 * 10; //10 MB
            $config['encrypt_name']         = true;

            $library_load                   = $this->load->library('upload', $config);

            //upload featured image
            if($_FILES['featured_image']['name'])
            {
                $config['upload_path'] = './data/images/';

                $this->upload->initialize($config);
                if ($this->upload->do_upload('featured_image')) 
                {
                    $uploaded               = $this->upload->data();
                    $featured_image         = $uploaded['file_name'];
                    $data['featured_image'] = $featured_image;
                } 
                else 
                {
                    // return false;
                    // echo $this->upload->display_errors();die;
                }
            }

            $subcategory_id = $this->subcategory->insert($data);

            if($subcategory_id)
            {
                $this->session->set_flashdata('message', 'success|insert sub category success');
                redirect(site_url('subcategories'), 'refresh');
            }
        }

        $categories = $this->category->where('deleted', '0');
        $categories = $this->category->find_all();

    	$this->template->set('title', 'Sub Category')
                        ->set('subtitle', 'Create Sub Category')
                        ->set('categories', $categories)
                        ->build('admin/add');
    }

    public function edit($id)
    {
        if($this->input->post())
        {
            $data = array(
            	'category_id' 	=> $this->input->post('category_id'),
                'name'          => $this->input->post('name'),
                'description'   => $this->input->post('description') 
            );

            $config['allowed_types']        = '*';
            $config['max_size']             = 1024 * 10; //10 MB
            $config['encrypt_name']         = true;

            $library_load                   = $this->load->library('upload', $config);

            //upload featured image
            if($_FILES['featured_image']['name'])
            {
                $config['upload_path'] = './data/images/';

                $this->upload->initialize($config);
                if ($this->upload->do_upload('featured_image')) 
                {
                    $uploaded               = $this->upload->data();
                    $featured_image         = $uploaded['file_name'];
                    $data['featured_image'] = $featured_image;
                } 
                else 
                {
                    // return false;
                    // echo $this->upload->display_errors();die;
                }
            }

            $subcategory_update = $this->subcategory->update($id, $data);

            if($subcategory_update)
            {
                $this->session->set_flashdata('message', 'success|update sub category success');
                redirect(site_url('subcategories/edit/'.$id), 'refresh');
            }
        }

        $categories = $this->category->where('deleted', '0');
        $categories = $this->category->find_all();
        $data 		= $this->subcategory->find($id);

        $this->template->set('title', 'Sub Category')
                        ->set('subtitle', 'Edit Sub Category')
                        ->set('data', $data)
                        ->set('categories', $categories)
                        ->build('admin/edit');
    }

    public function delete($id)
    {
        $subcategory_delete = $this->subcategory->set_soft_deletes(TRUE);
        $subcategory_delete = $this->subcategory->delete($id);

        if($subcategory_delete)
        {
            $this->session->set_flashdata('message', 'success|delete sub category success');
            redirect(site_url('subcategories'), 'refresh');
        }
    }
}

?>