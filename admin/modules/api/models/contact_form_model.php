<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class contact_form_model extends MY_Model {

	protected $table        = 'contact_form';
    protected $key          = 'id';
    protected $date_format  = 'datetime';
    protected $set_created  = true;

    public function __construct()
    {
        parent::__construct();
    }

}
