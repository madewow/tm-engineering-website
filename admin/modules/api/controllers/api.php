<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Api extends Api_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('api/contact_form_model', 'contact_form');
        $this->load->model('api/specialist_form_model', 'specialist_form');
        $this->load->model('api/subscription_form_model', 'subscription_form');

        //$this->check_login();
        
    }

    public function subscription_form()
    {
        $email  = trim($this->input->post('email'));

        $data = array(
            'email'     => $email
        );

        $subscription_id = $this->subscription_form->insert($data);

        if($subscription_id)
        {
            $this->rest->set_data($subscription_id);
        }
        else
        {
            $this->rest->set_error('error.');
        }

        $this->rest->render();
    }

    public function contact_form()
    {
    	$name 	= trim($this->input->post('name'));
    	$email 	= trim($this->input->post('email'));
    	$detail = trim($this->input->post('detail'));

    	$data = array(
    		'name' 		=> $name, 
    		'email' 	=> $email, 
    		'detail' 	=> $detail
    	);

    	$contact_id = $this->contact_form->insert($data);

    	if($contact_id)
    	{
    		$this->rest->set_data($contact_id);
    	}
    	else
    	{
    		$this->rest->set_error('error.');
    	}

    	$this->rest->render();
    }

    public function specialist_form()
    {
		$firstname 	= trim($this->input->post('firstname'));
    	$lastname 	= trim($this->input->post('lastname'));
    	$email 		= trim($this->input->post('email'));
    	$company 	= trim($this->input->post('company'));
    	$phone 		= trim($this->input->post('phone'));
    	$purchase_timeline = trim($this->input->post('purchase_timeline'));
    	$notes 		= trim($this->input->post('notes'));

    	$data = array(
    		'firstname' => $firstname, 
    		'lastname' 	=> $lastname, 
    		'email' 	=> $email,
    		'company' 	=> $company, 
    		'phone' 	=> $phone, 
    		'purchase_timeline' 	=> $purchase_timeline,
    		'notes' 	=> $notes
    	);

    	$specialist_form_id = $this->specialist_form->insert($data);

    	if($specialist_form_id)
    	{
    		$this->rest->set_data($specialist_form_id);
    	}
    	else
    	{
    		$this->rest->set_error('error.');
    	}

    	$this->rest->render();
    }
}