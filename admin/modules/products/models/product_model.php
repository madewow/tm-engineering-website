<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class product_model extends MY_Model {

	protected $table        = 'product';
    protected $table_cat    = 'category';
	protected $table_cat_sub = 'category_sub';
	protected $table_aut    = 'author';
    protected $key          = 'id';
    protected $date_format  = 'datetime';
    protected $set_created  = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function find_all()
    {
    	return $this->db->select($this->table.'.id, '.$this->table.'.name, '.$this->table_cat.'.name as category, '.$this->table.'.content, '.$this->table_cat_sub.'.name as category_sub, '.$this->table.'.status, '.$this->table_aut.'.name as author, '.$this->table.'.created_on')
    					->from($this->table)
    					->join($this->table_cat, $this->table_cat.'.id = '.$this->table.'.category_id')
                        ->join($this->table_aut, $this->table_aut.'.id = '.$this->table.'.author_id')
    					->join($this->table_cat_sub, $this->table_cat_sub.'.id = '.$this->table.'.category_sub_id')
    					->where($this->table.'.deleted', '0')
    					->where($this->table_cat.'.deleted', '0')
    					->where($this->table_aut.'.deleted', '0')
    					->order_by($this->table.'.id', 'desc')
    					->get()
    					->result();
    }

}
