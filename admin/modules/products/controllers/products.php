<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class products extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('products/product_model', 'product');
        $this->load->model('products/product_file_model', 'product_file');
        $this->load->model('products/product_image_model', 'product_image');
        $this->load->model('categories/category_model', 'category');
        $this->load->model('subcategories/subcategory_model', 'subcategory');

        $this->check_login();
    }

    public function index(){
        $lists = $this->product->find_all();

    	$this->template->set('title', 'Product')
                        ->set('subtitle', 'List Product')
                        ->set('lists', $lists)
                        ->build('admin/index');
    }

	public function add(){

		if($this->input->post())
		{
			$config['allowed_types']        = '*';
            $config['max_size']             = 1024 * 10; //10 MB
            // $config['encrypt_name']         = true;

            $library_load 					= $this->load->library('upload', $config);

            //upload featured image
            if($_FILES['featured_image']['name'])
            {
            	$config['upload_path'] = './data/images/';

            	$this->upload->initialize($config);
            	if ($this->upload->do_upload('featured_image')) {
	                $uploaded = $this->upload->data();
	            } else {
	                // return false;
	                // echo $this->upload->display_errors();die;
	            }
            }

			$name 				= $this->input->post('name');
			$category_id 		= $this->input->post('category_id');
			$category_sub_id 	= $this->input->post('category_sub_id');
			$content            = $this->input->post('content');
            $excerpt_title      = $this->input->post('excerpt_title');
			$excerpt 			= $this->input->post('excerpt');
			$product_file 		= $_FILES['product_file'];
			$featured_image 	= $uploaded['file_name'];
			$product_image 		= $_FILES['product_image'];
			$meta_description 	= $this->input->post('meta_description');

			$data = array(
				'author_id'			=> $this->session->userdata('admin')->id,
				'name' 				=> $name, 
				'category_id' 		=> $category_id, 
				'category_sub_id' 	=> ($category_sub_id ? $category_sub_id : NULL), 
				'content' 			=> $content, 
                'excerpt_title'     => $excerpt_title, 
				'excerpt' 			=> $excerpt, 
				// 'product_file' => $product_file, 
				'featured_image' 	=> $featured_image, 
				// 'product_image' => $product_image, 
				'meta_description' 	=> $meta_description,
                'status'            => $this->input->post('status')
			);

            $product_id = $this->product->insert($data);
            
            // upload file
            if($_FILES['product_file']['name'])
            {
            	$config['upload_path'] = './data/files/';

            	$this->upload->initialize($config);
            	foreach ($product_file['name'] as $key => $file) 
            	{
            		$_FILES['product_file[]']['name']		= $product_file['name'][$key];
		            $_FILES['product_file[]']['type']		= $product_file['type'][$key];
		            $_FILES['product_file[]']['tmp_name']	= $product_file['tmp_name'][$key];
		            $_FILES['product_file[]']['error']		= $product_file['error'][$key];
		            $_FILES['product_file[]']['size']		= $product_file['size'][$key];

            		if ($this->upload->do_upload('product_file[]')) {
		                $uploaded = $this->upload->data();

		                $data = array(
		                	'product_id' => $product_id, 
		                	'name' 		 => $uploaded['file_name']
		                );

		                $product_file_id = $this->product_file->insert($data);

		            } else {
		                // return false;
		                // echo $this->upload->display_errors();die;
		            }
            	}
            }

            // upload additional image
            if($_FILES['product_image']['name'])
            {
            	$config['upload_path'] = './data/images/';

            	$this->upload->initialize($config);
            	foreach ($product_image['name'] as $key => $file) 
            	{
            		$_FILES['product_image[]']['name']		= $product_image['name'][$key];
		            $_FILES['product_image[]']['type']		= $product_image['type'][$key];
		            $_FILES['product_image[]']['tmp_name']	= $product_image['tmp_name'][$key];
		            $_FILES['product_image[]']['error']		= $product_image['error'][$key];
		            $_FILES['product_image[]']['size']		= $product_image['size'][$key];

            		if ($this->upload->do_upload('product_image[]')) {
		                $uploaded = $this->upload->data();

		                $data = array(
		                	'product_id' => $product_id, 
		                	'name' 		 => $uploaded['file_name']
		                );

		                $product_image_id = $this->product_image->insert($data);
		            } else {
		                // return false;
		                // echo $this->upload->display_errors();die;
		            }
            	}
            }

            if($product_id)
            {
            	$this->session->set_flashdata('message', 'success|insert product success');
                redirect(site_url('products/add'), 'refresh');
            }
		}

		$categories = $this->category->where('deleted', '0');
        $categories = $this->category->find_all();

        $subcategories = $this->subcategory->find_all();

    	$this->template->set('title', 'Product')
    					->set('subtitle', 'Create Product')
    					->set('categories', $categories)
    					->set('subcategories', $subcategories)
    					->build('admin/add');
    }    

    public function edit($id){

		if($this->input->post())
		{
			$config['allowed_types']        = '*';
            $config['max_size']             = 1024 * 10; //10 MB
            // $config['encrypt_name']         = true;

            $library_load 					= $this->load->library('upload', $config);

            $name 				= $this->input->post('name');
			$category_id 		= $this->input->post('category_id');
			$category_sub_id 	= $this->input->post('category_sub_id');
            $content            = $this->input->post('content');
            $excerpt_title      = $this->input->post('excerpt_title');
			$excerpt 			= $this->input->post('excerpt');
			$product_file 		= $_FILES['product_file'];
			$product_image 		= $_FILES['product_image'];
			$meta_description 	= $this->input->post('meta_description');
			$status 			= $this->input->post('status');

			$data = array(
				'author_id'			=> $this->session->userdata('admin')->id,
				'name' 				=> $name, 
				'category_id' 		=> $category_id, 
				'category_sub_id' 	=> ($category_sub_id ? $category_sub_id : NULL), 
				'content' 			=> $content, 
                'excerpt_title'     => $excerpt_title,
				'excerpt' 			=> $excerpt, 
				'meta_description' 	=> $meta_description,
				'status' 			=> $status
			);

            //upload featured image
            if($_FILES['featured_image']['name'])
            {
            	$config['upload_path'] = './data/images/';

            	$this->upload->initialize($config);
            	if ($this->upload->do_upload('featured_image')) {
	                $uploaded = $this->upload->data();
	                $featured_image 		= $uploaded['file_name'];
	                $data['featured_image'] = $featured_image;
	            } else {
	                // return false;
	                // echo $this->upload->display_errors();die;
	            }
            }

            $product_id = $this->product->update($id, $data);
            
            // upload file
            if($_FILES['product_file']['name'])
            {
            	$config['upload_path'] = './data/files/';

            	$this->upload->initialize($config);
            	foreach ($product_file['name'] as $key => $file) 
            	{
            		$_FILES['product_file[]']['name']		= $product_file['name'][$key];
		            $_FILES['product_file[]']['type']		= $product_file['type'][$key];
		            $_FILES['product_file[]']['tmp_name']	= $product_file['tmp_name'][$key];
		            $_FILES['product_file[]']['error']		= $product_file['error'][$key];
		            $_FILES['product_file[]']['size']		= $product_file['size'][$key];

            		if ($this->upload->do_upload('product_file[]')) {
		                $uploaded = $this->upload->data();

		                $data = array(
		                	'product_id' => $id, 
		                	'name' 		 => $uploaded['file_name']
		                );

		                $product_file_id = $this->product_file->insert($data);

		            } else {
		                // return false;
		                // echo $this->upload->display_errors();die;
		            }
            	}
            }

            // upload additional image
            if($_FILES['product_image']['name'])
            {
            	$config['upload_path'] = './data/images/';

            	$this->upload->initialize($config);
            	foreach ($product_image['name'] as $key => $file) 
            	{
            		$_FILES['product_image[]']['name']		= $product_image['name'][$key];
		            $_FILES['product_image[]']['type']		= $product_image['type'][$key];
		            $_FILES['product_image[]']['tmp_name']	= $product_image['tmp_name'][$key];
		            $_FILES['product_image[]']['error']		= $product_image['error'][$key];
		            $_FILES['product_image[]']['size']		= $product_image['size'][$key];

            		if ($this->upload->do_upload('product_image[]')) {
		                $uploaded = $this->upload->data();

		                $data = array(
		                	'product_id' => $id, 
		                	'name' 		 => $uploaded['file_name']
		                );

		                $product_image_id = $this->product_image->insert($data);
		            } else {
		                // return false;
		                // echo $this->upload->display_errors();die;
		            }
            	}
            }

            if($product_id)
            {
            	$this->session->set_flashdata('message', 'success|update product success');
                redirect(site_url('products/edit/'.$id), 'refresh');
            }
		}

		$categories = $this->category->where('deleted', '0');
        $categories = $this->category->find_all();

        $subcategories = $this->subcategory->find_all();

        $product_file = $this->product_file->where('deleted', '0');
        $product_file = $this->product_file->find_all_by(array('product_id' => $id));

        $product_image = $this->product_image->where('deleted', '0');
        $product_image = $this->product_image->find_all_by(array('product_id' => $id));

        $data = $this->product->find($id);

    	$this->template->set('title', 'Product')
    					->set('subtitle', 'Create Product')
    					->set('categories', $categories)
    					->set('subcategories', $subcategories)
    					->set('product_file', $product_file)
    					->set('product_image', $product_image)
    					->set('data', $data)
    					->build('admin/edit');
    }    

    public function delete($id)
    {
    	$product_delete = $this->product->set_soft_deletes(TRUE);
        $product_delete = $this->product->delete($id);

        if($product_delete)
        {
            $this->session->set_flashdata('message', 'success|delete product success');
            redirect(site_url('products'), 'refresh');
        }
    }

    public function deletefile($product_id, $id)
    {
        $product_file_delete = $this->product_file->set_soft_deletes(TRUE);
        $product_file_delete = $this->product_file->delete($id);

        if($product_file_delete)
        {
            $this->session->set_flashdata('message', 'success|delete file success');
            redirect(site_url('products/edit/'.$product_id), 'refresh');
        }
    }

    public function deleteimage($product_id, $id)
    {
        $product_image_delete = $this->product_image->set_soft_deletes(TRUE);
        $product_image_delete = $this->product_image->delete($id);

        if($product_image_delete)
        {
            $this->session->set_flashdata('message', 'success|delete image success');
            redirect(site_url('products/edit/'.$product_id), 'refresh');
        }
    }
}

?>