<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class author_model extends MY_Model {

	protected $table        = 'author';
    protected $key          = 'id';
    protected $date_format  = 'datetime';
    protected $set_created  = true;

    public function __construct()
    {
        parent::__construct();
    }

    public function login($email, $password)
    {
        return $this->db->select('*, author.email as email')
                            ->from($this->table)
                            ->where('author.deleted','0')
                            ->where('author.email',$email)
                            ->where('password',sha1($password))
                            ->get()
                            ->result();
    }

}
