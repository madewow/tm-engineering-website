<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class authors extends Admin_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->model('home/home_model', 'home');
        $this->load->model('authors/author_model', 'authors');

        //$this->check_login();
        
    }

    public function index(){

        $this->check_login();

        $authors = $this->authors->find_all();

    	$this->template
                    ->set('authors',$authors)
                    ->build('admin/index');
    }

    public function signin(){
        
        $this->template
                    ->set_layout(null)
                    ->set('title','Admin')
                    ->build('admin/signin');
    }

    public function logout()
    {
        
        $this->session->unset_userdata('admin');
        $this->session->unset_userdata('admin_logged_in');
       
        redirect('signin', 'refresh');
    }

    public function login(){

        $username  = $this->input->post('username');
        $password  = $this->input->post('password');
            
        if(!empty($username) || !empty($password)){

            $login = $this->authors->login($username, $password);
            
            if(count($login) > 0){

                $this->session->set_userdata('admin_logged_in', true);
                $this->session->set_userdata('admin', $login[0]);
                
                redirect(site_url(), 'refresh');

            }else{

                $this->session->set_flashdata('message', 'error|Invalid email or password!');
                redirect('signin', 'refresh');
            }
        }
    }

    public function add(){

        $this->check_login();

    	$this->template
                    ->set('menu','author')
                    ->build('admin/add');
    }

    public function edit($id){

        $this->check_login();
        
        $authors = $this->authors->find($id);
        

        $this->template
                    
                    ->set('authors',$authors)
                   
                    ->build('admin/edit');
    }

    public function store($id=""){

        $name          = $this->input->post('name');
        $email         = $this->input->post('email');
        $password      = $this->input->post('password');
        $conf_password = $this->input->post('conf_password');
        


        if($password != $conf_password){

            $this->session->set_flashdata('message', 'error|Password and confirmed password didnt match');
            redirect('authors');

        }else{

            $data = array(

                "name" => $name,
                "email"    => $email,
                "password" => sha1($password)
                
                


            );

            if($id){

                $this->authors->update($id,$data);

            }else{

                $this->authors->insert($data);
            }

            $this->session->set_flashdata('message', 'success|Data saved successfully!');
            redirect('authors');

        }

        

        
    }

    public function delete($id){

        $data = array("deleted"=>1);
        $this->authors->update($id,$data);

        $this->session->set_flashdata('message', 'success|Data deleted successfully!');
        redirect('authors');
        
    }

    public function change_password(){

        $this->check_login();

        $this->template
                    ->set('module','general')
                    ->set('menu','setting')
                    ->set('submenu','change password')
                    ->set('title','Setting')
                    ->set('subtitle','Change Password')
                    ->build('admin/change_password');
        
    }

    public function update_password(){

        
        $curr_pass = $this->input->post('curr_pass');
        $new_pass = $this->input->post('new_pass');

        $admin_id = $this->session->userdata('admin')->id;

        $admin = $this->authors->find($admin_id);

        if($admin->password != sha1($curr_pass)){

            $this->session->set_flashdata('message', 'error| Invalid old password');
            redirect('authors/change_password');

        }else{

            $data = array("password" => sha1($new_pass));

            $this->authors->update($admin_id, $data);

            $this->session->set_flashdata('message', 'success| password change succesfully');
            redirect('authors/change_password');


        }
        
        
    }
    
}

?>